package Frames;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UnsupportedLookAndFeelException;

import traits.MenuBar;
import traits.Traits;

public class MainMenuFrame {
	
	/**
	 * creates the main menu JFrame with all the trait JButtons
	 * and the Generate button that generates all the NFT's
	 * and the corresponding metedata-files.
	 * 
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws UnsupportedLookAndFeelException
	 */
	public static void mainMenuFrame() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		JFrame mainMenuFrame = new FrameGenerator("Main Menu").getFrame();
		Font myFont1 = new Font("Serif", Font.BOLD, 30);
		new ButtonGenerator("Background", mainMenuFrame, 0,0);
		new ButtonGenerator("Creature", mainMenuFrame, 0 , 200);
		new ButtonGenerator("Mouth", mainMenuFrame, 0, 400);
		new ButtonGenerator("Eyes", mainMenuFrame, 0,600);
		new ButtonGenerator("Hair", mainMenuFrame, 200,0);
		new ButtonGenerator("Eyebrows", mainMenuFrame, 200,200);
		new ButtonGenerator("Assesories", mainMenuFrame, 200,400);
		JButton generateButton = new ButtonGenerator("Generate", mainMenuFrame, 200,600).getButton();
		generateButton.setForeground(Color.green.darker());
		generateButton.setBackground(Color.green);
		generateButton.setFont(myFont1);
		
		nftPictureDefaultValue(mainMenuFrame);
	
		
	}
	
	/**
	 * generates a BufferedImage of the complete nft with all the default values
	 * frame needs to be specified
	 * @param frame
	 */
	static void nftPictureDefaultValue(JFrame frame) {
		BufferedImage bufferedImage = new BufferedImage(780, 1688, BufferedImage.TYPE_INT_RGB);
		Traits traits = new Traits(780, 1688);
		bufferedImage = Traits.joinBufferedImage(traits.background(),
                traits.body(),
                traits.mouth(),
                traits.eyes(),
                traits.hair(),
                traits.alienAntenna(),
                traits.nose(),
                traits.eyebrows(),
                traits.glasses(),
                traits.clothes(),
                traits.accessories());
		
		frame.add(FrameGenerator.drawing(frame, FrameGenerator.resize(bufferedImage, 360, 800), "00", 0, 800, 410, 340));
	}
	
	

	


}
