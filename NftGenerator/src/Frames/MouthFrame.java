package Frames;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.UnsupportedLookAndFeelException;

import traits.Traits;

public class MouthFrame {
	
	
////////////////////COLORS /////////////////////////////
static Color black = new Color(0, 0, 0);
static Color white = new Color(255, 255, 255);
static Color red = new Color(211, 47, 47);
static Color yellow = new Color(255, 255, 0);
static Color brown = new Color(145, 122, 116);

////////////////////COLORS //////////////////////////////////
	
	
	Traits traits;
	
	public static BufferedImage bufferedImage = new BufferedImage(780, 1688, BufferedImage.TYPE_INT_RGB);
    static Graphics2D g2d = bufferedImage.createGraphics();
	
    /**
     * creates the mouth JFrame with JButtons; main menu, preview and save
     * 
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws UnsupportedLookAndFeelException
     */
	static void mouthFrame() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		JFrame mouthFrame = new FrameGenerator("Mouth").getFrame();
		new ButtonGenerator("Main Menu", mouthFrame, 0,0);
		new ButtonGenerator("Preview Mouth", mouthFrame, 0,400);
		new ButtonGenerator("Go Back", mouthFrame, 0,600).getButton().setForeground(new Color(46, 116, 43));	
	}
	

	static String metadataMouth;
	/**
	 * gets a random BufferedImage mouth and updates to the correct metatdata name
	 * @return mouth BufferedImage
	 */
	public static BufferedImage getMouth() {
		
		Random random = new Random();
		//MOUTH

        List<Color> toothColor = Arrays.asList(yellow, brown, brown, white, white);
        Color randomToothColor = toothColor.get(random.nextInt(toothColor.size()));
        g2d.setColor(black);
        g2d.fillRect(0, 0, 780, 1688);
        
        int randomMouthNumber = random.nextInt(7)+1;
		//munn med 4 tenner
        if(randomMouthNumber==1) {
        	metadataMouth = "Orc";
        	g2d.setColor(red);//leppe
            g2d.fillOval(270, 1100, 240, 100);
            g2d.fillOval(280, 1100, 220, 130);
            g2d.fillOval(290, 1070, 120, 130);
            g2d.fillOval(370, 1070, 120, 130);
        	g2d.setColor(Color.black);
            g2d.fillOval(280, 1100, 220, 100);
            g2d.setColor(randomToothColor);//tann
            g2d.fillRect(355, 1160, 30, 40);
            g2d.fillRect(395, 1160, 30, 40);
            g2d.fillOval(355, 1145, 30, 30);
            g2d.fillOval(395, 1145, 30, 30);
            g2d.fillRect(310, 1115, 30, 40);
            g2d.fillRect(440, 1115, 30, 40);
            g2d.fillOval(310, 1125, 30, 40);
            g2d.fillOval(440, 1125, 30, 40);
        }
      //hoggtann kvit
        else if(randomMouthNumber==2) {
        	metadataMouth = "Snake";
        	g2d.setColor(red);//leppe
            g2d.fillOval(270, 1085, 240, 120);
            g2d.fillOval(280, 1100, 220, 130);
            g2d.fillOval(290, 1070, 120, 130);
            g2d.fillOval(370, 1070, 120, 130);
        	g2d.setColor(Color.black);
            g2d.fillOval(280, 1100, 220, 100);
            g2d.setColor(randomToothColor);//tann
            g2d.fillRect(350, 1105, 30, 140);
            g2d.fillRect(400, 1105, 30, 140);
            g2d.fillOval(350, 1225, 30, 30);
            g2d.fillOval(400, 1232, 30, 30);
        }
      //retard kvit 
        else if(randomMouthNumber==3) {
        	metadataMouth = "Retarded";
        	g2d.setColor(red);//leppe
            g2d.fillOval(270, 1085, 240, 110);
        	g2d.setColor(Color.black);
            g2d.fillOval(290, 1100, 200, 80);
            g2d.setColor(randomToothColor);//tann
            g2d.fillRect(335, 1108, 30, 50);
            g2d.fillRect(375, 1102, 30, 50);
            g2d.fillRect(415, 1108, 30, 50);
            g2d.fillRect(455, 1124, 30, 25);
            g2d.fillRect(295, 1124, 30, 24); 
        }
      //vanlig 6 tann kvit
        else if(randomMouthNumber==4) {
        	metadataMouth = "Normal";
        	g2d.setColor(red);//leppe
            g2d.fillOval(270, 1085, 240, 110);
        	g2d.setColor(Color.black);
            g2d.fillOval(290, 1100, 200, 80);
            g2d.setColor(randomToothColor);//tann
            g2d.fillRect(335, 1108, 30, 25);
            g2d.fillRect(375, 1102, 30, 25);
            g2d.fillRect(415, 1108, 30, 25);
            g2d.fillOval(335, 1112, 30, 25);
            g2d.fillOval(375, 1108, 30, 25);
            g2d.fillOval(415, 1112, 30, 25);
            g2d.fillRect(335, 1148, 30, 25);
            g2d.fillRect(375, 1152, 30, 25);
            g2d.fillRect(415, 1148, 30, 25);
            g2d.fillOval(335, 1144, 30, 25);
            g2d.fillOval(375, 1146, 30, 25);
            g2d.fillOval(415, 1144, 30, 25);
        }
      //britisk kvit
        else if(randomMouthNumber==5) {
        	metadataMouth = "British";
        	g2d.setColor(Color.black);
            g2d.fillOval(320, 1040, 140, 240);
        	g2d.setColor(red);//leppe
            g2d.fillOval(320, 1045, 140, 230);
        	g2d.setColor(Color.black);
            g2d.fillOval(340, 1060, 100, 200);
            g2d.setColor(randomToothColor);//tann
            g2d.fillRect(357, 1088, 12, 146);
            g2d.fillRect(375, 1068, 12, 186);
            g2d.fillRect(393, 1068, 12, 186);
            g2d.fillRect(411, 1088, 12, 146);
            g2d.setColor(Color.black);
            g2d.fillRect(355, 1150, 14, 25);
            g2d.fillRect(375, 1170, 14, 40);
            g2d.fillRect(392, 1140, 14, 45);
            g2d.fillRect(410, 1150, 14, 15);
        }
      //underbitt kvit
        else if(randomMouthNumber==6) {
        	metadataMouth = "Underbite";
        	g2d.setColor(red);//leppe
            g2d.fillOval(270, 1085, 240, 120);
        	g2d.setColor(Color.black);
            g2d.fillOval(290, 1100, 200, 90);
            g2d.setColor(randomToothColor);//tann
            g2d.fillRect(335, 1138, 30, 42);
            g2d.fillRect(375, 1150, 30, 40);
            g2d.fillRect(415, 1138, 30, 42);
            g2d.fillRect(455, 1128, 30, 25);
            g2d.fillRect(295, 1128, 30, 24); 
        }
      //full tann kvit
        else if(randomMouthNumber==7) {
        	metadataMouth = "All teeths";
        	g2d.setColor(red);//leppe
            g2d.fillOval(270, 1085, 240, 120);
        	g2d.setColor(Color.black);
            g2d.fillOval(290, 1100, 200, 90);
            g2d.setColor(randomToothColor);//tann
            g2d.fillRect(335, 1150, 30, 30);
            g2d.fillRect(375, 1155, 30, 30);
            g2d.fillRect(415, 1150, 30, 30);
            g2d.fillRect(455, 1128, 30, 25);
            g2d.fillRect(295, 1128, 30, 24);
            g2d.fillRect(335, 1110, 30, 30);
            g2d.fillRect(375, 1105, 30, 30);
            g2d.fillRect(415, 1110, 30, 30);
        }
        g2d.setColor(Color.white);
        Font font = new Font("Serif", Font.BOLD, 20);
        g2d.setFont(font);
        g2d.drawString(metadataMouth, 360, 850);
		return bufferedImage;
	}

}
