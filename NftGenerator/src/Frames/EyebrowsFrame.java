package Frames;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.UnsupportedLookAndFeelException;

public class EyebrowsFrame {
	
	public static BufferedImage bufferedImage = new BufferedImage(780, 1688, BufferedImage.TYPE_INT_RGB);
    static Graphics2D g2d = bufferedImage.createGraphics();
	
    /**
     * creates the eyebrowsframe with JButtons
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws UnsupportedLookAndFeelException
     */
	static void eyebrowsFrame() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		JFrame eyebrowsFrame = new FrameGenerator("Eyebrows").getFrame();
		new ButtonGenerator("Main Menu", eyebrowsFrame, 0,0);
		new ButtonGenerator("Preview Eyebrows", eyebrowsFrame, 0,400);
		new ButtonGenerator("Go Back", eyebrowsFrame, 0,600).getButton().setForeground(new Color(46, 116, 43));
		
		
	}
	
	public static String metadataEyebrows = "";
	
	/**
	 * gets a random BufferedImage pair of eyebrows and updates to the correct metatdata name
	 * @return eyebrows BufferImage
	 */
	public static BufferedImage getEyebrows() {
			Random random = new Random();
			//String metadataEyebrows = "";
			int randomEyelashNumber = random.nextInt(10)+1;
			g2d.setColor(Color.white);
			g2d.fillRect(0, 0, 780, 1688);
	            if(randomEyelashNumber==1) {
	            	 metadataEyebrows = "Normal";
	            	for(int i=0; i<160; i=i+1) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 670-(i/30), 20, 25);
	                    g2d.fillRect(420+i, 670-(i/30), 20, 25);
	                }  
	            }
	            else if(randomEyelashNumber==2) {
	            	 metadataEyebrows = "Angry";
	                for(int i=0; i<160; i=i+10) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 690-(i/2), 20, 25);
	                    g2d.fillRect(420+i, 690-(i/2), 20, 25);
	                }    
	            }
	            else if(randomEyelashNumber==3) {
	            	 metadataEyebrows = "Sad";
	                for(int i=0; i<160; i=i+10) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 615+(i/2), 20, 25);
	                    g2d.fillRect(420+i, 615+(i/2), 20, 25);
	                }    
	            }
	            else if(randomEyelashNumber==4) {
	            	 metadataEyebrows = "Unhappy";
	                for(int i=0; i<160; i=i+10) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 640+(i/5), 20, 25);
	                    g2d.fillRect(420+i, 640+(i/5), 20, 25);
	                }    
	            }
	            else if(randomEyelashNumber==5) {
	            	 metadataEyebrows = "Superangry";
	                for(int i=0; i<140; i=i+10) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 710-(i), 25, 25);
	                    g2d.fillRect(420+i, 710-(i), 25, 25);
	                }    
	            }
	            else if(randomEyelashNumber==6) {
	            	 metadataEyebrows = "Casual";
	            	for(int i=0; i<80; i=i+10) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 670-(i/20), 20, 25);
	                    g2d.fillRect(420+i, 670-(i/20), 20, 25);
	                    g2d.fillRect(260-i, 670+(i/3), 20, 25);
	                    g2d.fillRect(500+i, 670+(i/3), 20, 25);
	                } 
	            }
	            else if(randomEyelashNumber==7) {
	            	 metadataEyebrows = "Casual v2";
	            	for(int i=0; i<35; i=i+10) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 660+(i/3), 20, 25);
	                    g2d.fillRect(420+i, 660+(i/3), 20, 25);
	                    g2d.fillRect(290-i, 670-(i/3), 20, 25);
	                    g2d.fillRect(470+i, 670-(i/3), 20, 25);
	                    g2d.fillRect(240-i, 660+(i/3), 20, 25);
	                    g2d.fillRect(520+i, 660+(i/3), 20, 25);
	                    g2d.fillRect(190-i, 670-(i/3), 20, 25);
	                    g2d.fillRect(570+i, 670-(i/3), 20, 25);
	                }    
	            }
	            else if(randomEyelashNumber==8) {
	            	  metadataEyebrows = "Normal v2";
	            	for(int i=0; i<35; i=i+10) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 680-(i/3), 20, 25);
	                    g2d.fillRect(420+i, 680-(i/3), 20, 25);
	                    g2d.fillRect(290-i, 670-(i/3), 20, 25);
	                    g2d.fillRect(470+i, 670-(i/3), 20, 25);
	                    g2d.fillRect(240-i, 660+(i/3), 20, 25);
	                    g2d.fillRect(520+i, 660+(i/3), 20, 25);
	                    g2d.fillRect(190-i, 670+(i/3), 20, 25);
	                    g2d.fillRect(570+i, 670+(i/3), 20, 25);
	                }
	            }
	            else if(randomEyelashNumber==9) {
	            	 metadataEyebrows = "Shocked";
	            	for(int i=0; i<35; i=i+10) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 640+(i), 20, 25);
	                    g2d.fillRect(420+i, 640+(i), 20, 25);
	                    g2d.fillRect(290-i, 670-(i/3), 20, 25);
	                    g2d.fillRect(470+i, 670-(i/3), 20, 25);
	                    g2d.fillRect(240-i, 660+(i/3), 20, 25);
	                    g2d.fillRect(520+i, 660+(i/3), 20, 25);
	                    g2d.fillRect(190-i, 670+(i), 20, 25);
	                    g2d.fillRect(570+i, 670+(i), 20, 25);
	                }   
	            }
	            else if(randomEyelashNumber==10) {
	            	 metadataEyebrows = "Disappointed";
	            	for(int i=0; i<35; i=i+10) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 640+(i), 20, 25);
	                    g2d.fillRect(420+i, 640+(i), 20, 25);
	                    g2d.fillRect(290-i, 670+(i/3), 20, 25);
	                    g2d.fillRect(470+i, 670+(i/3), 20, 25);
	                    g2d.fillRect(240-i, 680+(i), 20, 25);
	                    g2d.fillRect(520+i, 680+(i), 20, 25);
	                    g2d.fillRect(190-i, 710-(i), 20, 25);
	                    g2d.fillRect(570+i, 710-(i), 20, 25);
	                }    
	            }
	            
	            g2d.setColor(Color.black);
	            Font font = new Font("Serif", Font.BOLD, 20);
	            g2d.setFont(font);
	            
				g2d.drawString(metadataEyebrows, 350, 850);
			return bufferedImage;
	
	}

}
