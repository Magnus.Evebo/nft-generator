package Frames;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.UnsupportedLookAndFeelException;

import traits.RainbowColors;

public class EyesFrame {
	
	public static BufferedImage bufferedImage = new BufferedImage(780, 1688, BufferedImage.TYPE_INT_RGB);
    static Graphics2D g2d = bufferedImage.createGraphics();
	
    /**
     * creates the Eyes frame with JButtons; main menu, preview and save
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws UnsupportedLookAndFeelException
     */
	static void eyesFrame() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		JFrame eyesFrame = new FrameGenerator("Eyes").getFrame();
		new ButtonGenerator("Main Menu", eyesFrame, 0,0);
		new ButtonGenerator("Preview Eyes", eyesFrame, 0,400);
		new ButtonGenerator("Go Back", eyesFrame, 0,600).getButton().setForeground(new Color(46, 116, 43));	
	}


	static String metadataEye = "";
	/**
	 * gets a random BufferedImage pair of eyes and updates to the correct metatdata name
	 * @return eyes BufferedImage
	 */
	public static BufferedImage getEyes() {
        Color randomEyeColor = RainbowColors.getRandomColor(RainbowColors.allColors);
        Random random = new Random();
        int randomEyeNumber = random.nextInt(11)+1;
        g2d.setColor(Color.black);
        g2d.fillRect(0, 0, 780, 1688);
        
        if(randomEyeNumber==1) {
        	metadataEye = "Up and down";
        	g2d.setColor(Color.black);
            g2d.fillOval(217, 698, 125, 175);
            g2d.fillOval(438, 719, 123, 174);
            g2d.setColor(Color.white);
            g2d.fillOval(220, 700, 120, 190);
            g2d.fillOval(440, 700, 120, 190);
            g2d.setColor(randomEyeColor);
            g2d.fillOval(246, 704, 60, 70);
            g2d.fillOval(464, 815, 60, 70);
            g2d.setColor(Color.black);
            g2d.fillOval(260, 715, 32, 40);
            g2d.fillOval(477, 835, 32, 40);
            g2d.setColor((new Color(211, 47, 47)));
            g2d.drawString("/////////////", 260, 890);
            g2d.drawString("//////////////////////", 248, 880);
            g2d.drawString("/////                 /////", 242, 870);
            g2d.drawString("/////                 /////", 462, 730);
            g2d.drawString("/////////////", 480, 710);
            g2d.drawString("//////////////////////", 468, 720);
        }
        else if(randomEyeNumber==2) {
        	metadataEye = "Narrow";
        	g2d.setColor(Color.black );
            g2d.fillOval(187, 766, 186, 37);
            g2d.fillOval(407, 766, 186, 37);
        	g2d.setColor(Color.white);
            g2d.fillOval(190, 767, 180, 35);
            g2d.fillOval(410, 767, 180, 35);
            g2d.setColor(randomEyeColor);
            g2d.fillOval(310, 767, 35, 35);
            g2d.fillOval(435, 767, 35, 35);
            g2d.setColor(Color.black);
            g2d.fillOval(327, 777, 15, 15);
            g2d.fillOval(438, 777, 15, 15);
            g2d.setColor((new Color(211, 47, 47)));
            g2d.fillRect(192, 779, 15, 11);
            g2d.fillRect(573, 779, 15, 11);
        }
        else if(randomEyeNumber==3) {
        	metadataEye = "Up and normal";
        	g2d.setColor(Color.black);
            g2d.fillOval(217, 698, 125, 175);
            g2d.fillOval(437, 708, 125, 174);
            g2d.setColor(Color.white);
            g2d.fillOval(220, 700, 120, 190);
            g2d.fillOval(440, 700, 120, 190);
            g2d.setColor(randomEyeColor);
            g2d.fillOval(246, 704, 60, 70);
            g2d.fillOval(466, 747, 60, 70);
            g2d.setColor(Color.black);
            g2d.fillOval(260, 715, 32, 40);
            g2d.fillOval(480, 760, 32, 40);
            g2d.setColor((new Color(211, 47, 47)));
            g2d.drawString("/////////////", 260, 890);
            g2d.drawString("//////////////////////", 248, 880);
            g2d.drawString("/////                 /////", 242, 870);

        }
        else if(randomEyeNumber==4) {
        	metadataEye = "Big normal";
        	g2d.setColor(Color.black);
            g2d.fillOval(217, 698, 125, 175);
            g2d.fillOval(438, 700, 124, 165);
            g2d.setColor(Color.white);
            g2d.fillOval(220, 700, 120, 190);
            g2d.fillOval(440, 700, 120, 190);
            g2d.setColor(Color.black);
            g2d.fillOval(234, 740, 94, 116);
            g2d.fillOval(454, 740, 94, 116);
            g2d.setColor(randomEyeColor);
            g2d.fillOval(236, 740, 90, 110);
            g2d.fillOval(456, 740, 90, 110);
            g2d.setColor(Color.black);
            g2d.fillOval(260, 770, 32, 40);
            g2d.fillOval(480, 770, 32, 40);
            g2d.setColor((new Color(211, 47, 47)));
            g2d.drawString("/////////////", 260, 890);
            g2d.drawString("//////////////////////", 248, 880);
            g2d.drawString("/////                 /////", 242, 870);
            g2d.drawString("////////////", 485, 711);
            g2d.drawString("/////////////////////", 470, 721);
            g2d.drawString("////////////", 485, 731);
        }
        else if(randomEyeNumber==5) {
        	metadataEye = "Small";
        	g2d.setColor(Color.black);
            g2d.fillOval(205, 755, 143, 53);
            g2d.fillOval(425, 755, 143, 53);
            g2d.setColor(Color.white);
            g2d.fillOval(205, 755, 145, 50);
            g2d.fillOval(425, 755, 145, 50);
            g2d.setColor(randomEyeColor);
            g2d.fillOval(253, 755, 56, 50);
            g2d.fillOval(473, 755, 56, 50);
            g2d.setColor(Color.black);
            g2d.fillOval(263, 763, 34, 34);
            g2d.fillOval(483, 763, 34, 34);
        }
        else if(randomEyeNumber==6) {
        	metadataEye = "Lizzard";
        	g2d.setColor(Color.black);
            g2d.fillOval(205, 770, 143, 53);
            g2d.fillOval(425, 770, 143, 53);
            g2d.fillOval(205, 736, 143, 53);
            g2d.fillOval(425, 736, 143, 53);
            g2d.setColor(Color.white);
            g2d.fillOval(205, 745, 145, 70);
            g2d.fillOval(425, 745, 145, 70);
            g2d.setColor(Color.black);
            g2d.fillOval(259, 745, 43, 73);
            g2d.fillOval(479, 745, 43, 73);
            g2d.setColor(randomEyeColor);
            g2d.fillOval(259, 745, 40, 70);
            g2d.fillOval(479, 745, 40, 70);
            g2d.setColor(Color.black);
            g2d.fillOval(271, 763, 14, 34);
            g2d.fillOval(491, 763, 14, 34);
        }
        else if(randomEyeNumber==7) {
        	metadataEye = "Big iris";
        	g2d.setColor(Color.black);
            g2d.fillOval(180, 740, 197, 100);
            g2d.fillOval(400, 740, 197, 100);
            g2d.setColor(Color.white);
            g2d.fillOval(190, 740, 180, 100);
            g2d.fillOval(410, 740, 180, 100);
            g2d.setColor(Color.black);
            g2d.fillOval(234, 740, 94, 116);
            g2d.fillOval(454, 740, 94, 116);
            g2d.setColor(randomEyeColor);
            g2d.fillOval(236, 740, 90, 110);
            g2d.fillOval(456, 740, 90, 110);
            g2d.setColor(Color.black);
            g2d.fillOval(260, 770, 32, 40);
            g2d.fillOval(480, 770, 32, 40);
            g2d.setColor(randomEyeColor);
            g2d.fillOval(267, 780, 20, 20);
            g2d.fillOval(487, 780, 20, 20);
            g2d.setColor(Color.black);
            g2d.fillOval(274, 787, 6, 8);
            g2d.fillOval(494, 787, 6, 8);
        }
      //ser utover
        else if(randomEyeNumber==8) {
        	metadataEye = "Looking outwards";
        	g2d.setColor(Color.black);
            g2d.fillOval(217, 700, 123, 193);
            g2d.fillOval(440, 700, 123, 193);
            g2d.setColor(Color.white);
            g2d.fillOval(220, 700, 120, 190);
            g2d.fillOval(440, 700, 120, 190);
            g2d.setColor(randomEyeColor);
            g2d.fillOval(225, 755, 60, 70);
            g2d.fillOval(495, 755, 60, 70);
            g2d.setColor(Color.black);
            g2d.fillOval(235, 770, 32, 40);
            g2d.fillOval(513, 770, 32, 40);
        }
      //ser innover
        else if(randomEyeNumber==9) {
        	metadataEye = "Looking inwards";
        	g2d.setColor(Color.black);
            g2d.fillOval(220, 700, 123, 193);
            g2d.fillOval(437, 700, 123, 193);
            g2d.setColor(Color.white);
            g2d.fillOval(220, 700, 120, 190);
            g2d.fillOval(440, 700, 120, 190);
            g2d.setColor(randomEyeColor);
            g2d.fillOval(275, 755, 60, 70);
            g2d.fillOval(445, 755, 60, 70);
            g2d.setColor(Color.black);
            g2d.fillOval(295, 770, 32, 40);
            g2d.fillOval(455, 770, 32, 40);
        }
        //ser nedover
        else if(randomEyeNumber==10) {
        	metadataEye = "Looking downwards";
        	g2d.setColor(Color.black);
        	g2d.fillOval(220, 700, 120, 194);
            g2d.fillOval(440, 700, 120, 194);
            g2d.setColor(Color.white);
            g2d.fillOval(220, 700, 120, 190);
            g2d.fillOval(440, 700, 120, 190);
            g2d.setColor(randomEyeColor);
            g2d.fillOval(265, 805, 60, 70);
            g2d.fillOval(455, 805, 60, 70);
            g2d.setColor(Color.black);
            g2d.fillOval(281, 825, 32, 40);
            g2d.fillOval(467, 825, 32, 40);
        }
        else if(randomEyeNumber==11) {
        	metadataEye = "Looking upwnwards";
        	g2d.setColor(Color.black);
        	g2d.fillOval(220, 697, 120, 194);
            g2d.fillOval(440, 697, 120, 194);
            g2d.setColor(Color.white);
            g2d.fillOval(220, 700, 120, 190);
            g2d.fillOval(440, 700, 120, 190);
            g2d.setColor(randomEyeColor);
            g2d.fillOval(246, 704, 60, 70);
            g2d.fillOval(466, 704, 60, 70);
            g2d.setColor(Color.black);
            g2d.fillOval(260, 715, 32, 40);
            g2d.fillOval(480, 715, 32, 40);
            g2d.setColor((new Color(211, 47, 47)));
            g2d.drawString("/////////////", 260, 890);
            g2d.drawString("//////////////////////", 248, 880);
            g2d.drawString("/////                 /////", 242, 870);
            g2d.drawString("/////////////", 480, 890);
            g2d.drawString("//////////////////////", 468, 880);
            g2d.drawString("/////                 /////", 462, 870);
        }
        g2d.setColor(Color.white);
        Font font = new Font("Serif", Font.BOLD, 20);
        g2d.setFont(font);
        g2d.drawString(metadataEye, 330, 910);
		return bufferedImage;
	}

}
