package Frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;


import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import traits.Generate;
import traits.MenuBar;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;



public class FrameGenerator extends JFrame{
	
	
	
	public JFrame frame;

	 /**
	  * draws a BufferedImage on a JButton with the metadata text given.
	  * the button with the metadata text is 180x180
	  * 
	  * @param frame
	  * @param image
	  * @param buttonText
	  * @return
	  */
	public static JButton drawingMeta(JFrame frame, BufferedImage image, String buttonText) {
    	ImageIcon picture = new ImageIcon();
		JLabel label = new JLabel();
		picture.setImage(image);
		label.setIcon(picture);
		JButton buttonBig =new ButtonGenerator(buttonText, frame, 10, 210, 180, 180).getButton();
		buttonBig.setIcon(picture);
		return buttonBig;
    }
	
	
	
	/**
	 * Generates a new frame with buttons and chooses the correct sliders
	 * @param frameText
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws UnsupportedLookAndFeelException
	 */
	public FrameGenerator(String frameText) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		frame = new JFrame(frameText);
		
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(400,863));
		sliderChooser(frameText, panel);
		
		frame.add(panel, BorderLayout.EAST);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1200, 863);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setLayout(null);
		MenuBar.MenuBar(frame);

		frame.revalidate();
	}
	




	/**
	 * Chooses the correct sliders based on the frameText 
	 * @param frameText
	 * @param panel
	 */
	public void sliderChooser(String frameText, JPanel panel) {
		if(frameText.equals("Background")) {
			backgroundSliders(panel);
		}
		if(frameText.equals("Creature")) {
			creatureSliders(panel);
		}
		if(frameText.equals("Mouth")) {
			mouthSliders(panel);
		}
		if(frameText.equals("Eyes")) {
			eyesSliders(panel);
		}
		if(frameText.equals("Hair")) {
			hairSliders(panel);
		}
		if(frameText.equals("Eyebrows")) {
			eyebrowsSliders(panel);
		}
		if(frameText.equals("Assesories")) {
			assesoriesSliders(panel);
		}
		if(frameText.equals("Main Menu")) {
			mainMenuSliders(panel);
		}
	}
	
	
	//default value = 10
	public static int numberOfNFTS = 10;
	/**
	 * creates the mainMenu JSlider and decides the number of NFT's
	 * beeing generated based on the users input
	 * @param panel
	 */
	public void mainMenuSliders(JPanel panel) {
		JSlider slider1 = sliderGeneratorMainMenu(panel, 1, "Number of NFT's");
		slider1.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				numberOfNFTS = source.getValue();
				}});
		}
	
	
	


	//default value
	public static int staticb = 1;
	
	public static int pattern, squares, sircles, rectangles;
	/** 
	 * generates the background sliders with the correct JSlider ChangeListner,
	 * The method updates the 5 field variables; pattern, squares, circles, rectangles
	 * based on the users decision
	 * @param panel
	 */
	public void backgroundSliders(JPanel panel) {
		JSlider slider1 = sliderGenerator(panel, 5, "Pattern");
		slider1.addChangeListener(new ChangeListener() {
			

			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				pattern = source.getValue();}});
		
		
		JSlider slider2 = sliderGenerator(panel, 5, "Squares");
		slider2.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				squares = source.getValue();}});
		
		
		JSlider slider3 = sliderGenerator(panel, 5, "Static");
		slider3.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				staticb = source.getValue();}});
		
		
		JSlider slider4 = sliderGenerator(panel, 5, "sircles");
		slider4.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				sircles = source.getValue();}});
		
		
		JSlider slider5 = sliderGenerator(panel, 5, "Rectangle");
		slider5.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				rectangles = source.getValue();}});
		
		}
	
	
	//default value
	public static int human = 1;
	
	public static int alien, zombie;
	/**
	 * generates the creature sliders with the correct JSlider ChangeListner,
	 * The method updates the 3 field variables; human, alien, zombie.
	 * based on the users decision
	 * @param panel
	 */
	public void creatureSliders(JPanel panel) {
		JSlider slider1 = sliderGenerator(panel, 3, "Human");
		slider1.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				human = source.getValue();}});
		
		JSlider slider2 = sliderGenerator(panel, 3, "Alien");
		slider2.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				alien = source.getValue();}});
		
		JSlider slider3 = sliderGenerator(panel, 3, "Zombie");
		slider3.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				zombie = source.getValue();}});
		}
	
	
	
	//default value
	public static int normal = 1;
	public static int orc, snake, retarded, british, underbite, allTeeths;
	/**
	 * generates the mouth sliders with the correct JSlider ChangeListner,
	 * The method updates the 7 field variables; orc, snake, retarded, normal, british, underbite, all teeths.
	 * based on the users decision
	 * @param panel
	 */
	public void mouthSliders(JPanel panel) {
		JSlider slider1 = sliderGenerator(panel, 7, "Orc");
		slider1.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				orc = source.getValue();}});
		
		JSlider slider2 = sliderGenerator(panel, 7, "Snake");
		slider2.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				snake = source.getValue();}});
		
		JSlider slider3 = sliderGenerator(panel, 7, "Retarded");
		slider3.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				retarded = source.getValue();}});
		
		JSlider slider4 = sliderGenerator(panel, 7, "Normal");
		slider4.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				normal = source.getValue();}});
		
		JSlider slider5 = sliderGenerator(panel, 7, "British");
		slider5.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				british = source.getValue();}});
		
		JSlider slider6 = sliderGenerator(panel, 7, "Underbite");
		slider6.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				underbite = source.getValue();}});
		
		JSlider slider7 = sliderGenerator(panel, 7, "All teeths");
		slider7.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				allTeeths = source.getValue();}});
		}
	
	//default value
	public static int bigNormal = 1;
	
	public static int upAndDown, narrow, upAndNormal, small, lizzard,
	    bigIris, lookingOutwards, lookingInwards, lookingDownwards, lookingUpwards;
	/**
	 * generates the eye sliders with the correct JSlider ChangeListner,
	 * The method updates the 11 field variables; upAndDown, narrow, upAndNormal, bigNormal, small,
	 * lizzard, bigIris, lookingOutwards, lookingInwards, lookingDownwards, lookingUpwards.
	 * based on the users decision
	 * @param panel
	 */
	public void eyesSliders(JPanel panel) {
		JSlider slider1 = sliderGenerator(panel, 11, "Up and down");
		slider1.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				upAndDown = source.getValue();}});
		
		JSlider slider2 = sliderGenerator(panel, 11, "Narrow");
		slider2.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				narrow = source.getValue();}});
		
		JSlider slider3 = sliderGenerator(panel, 11, "Up and normal");
		slider3.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				upAndNormal = source.getValue();}});
		
		JSlider slider4 = sliderGenerator(panel, 11, "Big normal");
		slider4.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				bigNormal = source.getValue();}});
		
		JSlider slider5 = sliderGenerator(panel, 11, "small");
		slider5.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				small = source.getValue();}});
		
		JSlider slider6 = sliderGenerator(panel, 11, "Lizzard");
		slider6.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				lizzard = source.getValue();}});
		
		JSlider slider7 = sliderGenerator(panel, 11, "Big iris");
		slider7.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				bigIris = source.getValue();}});
		
		JSlider slider8 = sliderGenerator(panel, 11, "Looking outwards");
		slider8.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				lookingOutwards = source.getValue();}});
		
		JSlider slider9 = sliderGenerator(panel, 11, "Looking inwards");
		slider9.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				lookingInwards = source.getValue();}});
		
		JSlider slider10 = sliderGenerator(panel, 11, "Looking downwards");
		slider10.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				lookingDownwards = source.getValue();}});
		
		JSlider slider11 = sliderGenerator(panel, 11, "Looking upwnwards");
		slider11.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				lookingUpwards = source.getValue();}});
		}
	
	
	//default value
	public static int curly = 1;
	
	public static int ziggZagg, pherb, jimmy;
	/**
	 * generates the hair sliders with the correct JSlider ChangeListner,
	 * The method updates the 4 field variables; ziggZagg, curly, pherp, jimmy.
	 * based on the users decision
	 * @param panel
	 */
	public void hairSliders(JPanel panel) {
		JSlider slider1 = sliderGenerator(panel, 4, "Zigg zagg");
		slider1.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				ziggZagg = source.getValue();}});
		
		JSlider slider2 = sliderGenerator(panel, 4, "Curly");
		slider2.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				curly = source.getValue();}});
		
		JSlider slider3 = sliderGenerator(panel, 4, "Pherb");
		slider3.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				pherb = source.getValue();}});
		
		JSlider slider4 = sliderGenerator(panel, 4, "Jimmy");
		slider4.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				jimmy = source.getValue();}});
		}
	
	
	//default value
	public static int normalEyebrow =1;
	
	public static int angry, sad, unhappy, superAngry, casual,
	                  casualv2, normalv2, shocked, dissappointed;
	/**
	 * generates the eyebrow sliders with the correct JSlider ChangeListner,
	 * The method updates the 10 field variables; normalEyebrow, angry, sad, unhappy, superangry, casual,
	 *  casualv2, normalv2, shocked, dissapointed.
	 * based on the users decision
	 * @param panel
	 */
	public void eyebrowsSliders(JPanel panel) {
		JSlider slider1 = sliderGenerator(panel, 10, "Normal");
		slider1.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				normalEyebrow = source.getValue();}});
		
		JSlider slider2 = sliderGenerator(panel, 10, "Angry");
		slider2.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				angry = source.getValue();}});
		
		JSlider slider3 = sliderGenerator(panel, 10, "Sad");
		slider3.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				sad = source.getValue();}});
		
		JSlider slider4 = sliderGenerator(panel, 10, "Unhappy");
		slider4.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				unhappy = source.getValue();}});
		
		JSlider slider5 = sliderGenerator(panel, 10, "Superangry");
		slider5.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				superAngry = source.getValue();}});
		
		JSlider slider6 = sliderGenerator(panel, 10, "Casual");
		slider6.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				casual = source.getValue();}});
		
		JSlider slider7 = sliderGenerator(panel, 10, "Casual v2");
		slider7.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				casualv2 = source.getValue();}});
		
		JSlider slider8 = sliderGenerator(panel, 10, "Normal v2");
		slider8.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				normalv2 = source.getValue();}});
		
		JSlider slider9 = sliderGenerator(panel, 10, "Shocked");
		slider9.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				shocked = source.getValue();}});
		
		JSlider slider10 = sliderGenerator(panel, 10, "Disappointed");
		slider10.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				dissappointed = source.getValue();}});
		}
	
	
	public static int normalGlasses, cruckedGlasses, jordans;
	/**
	 * generates the assesories sliders with the correct JSlider ChangeListner,
	 * The method updates the 3 int field variables; normalGlasses, cruckedGlasses, jordans.
	 * based on the users decision
	 * @param panel
	 */
	public void assesoriesSliders(JPanel panel) {
		JSlider slider1 = sliderGenerator(panel, 3, "Normal glasses");
		slider1.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				normalGlasses = source.getValue();}});
		
		JSlider slider2 = sliderGenerator(panel, 3, "Crucked glasses");
		slider2.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				cruckedGlasses = source.getValue();}});
		
		JSlider slider3 = sliderGenerator(panel, 3, "Jordans");
		slider3.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				jordans = source.getValue();}});
		}
	
	
	/**
	 * Generates a horizontal JSliden that starts at 5 with the interval 0-10.
	 * need to specify what JPanel it should be attached to and also how many
	 * JSliders there totaly should be on the page (int) and a String with the
	 * textlabel
	 * @param panel
	 * @param number
	 * @param labelText
	 * @return
	 */
	public JSlider sliderGenerator(JPanel panel, int number, String labelText) {
		JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 10, 5);
		JLabel label = new JLabel();
		label.setFont(new Font("Verdana", Font.PLAIN, 18));
		label.setText(labelText);
		slider.setMajorTickSpacing(1);
		//slider.setMinorTickSpacing(1);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		panel.setLayout(new GridLayout(number,1));
		panel.add(label);
		panel.add(slider);
		return slider;
	}
	
	
	public JSlider sliderGeneratorMainMenu(JPanel panel, int number, String labelText) {
		JSlider slider = new JSlider(JSlider.VERTICAL, 0, 1000, 500);
		JLabel label = new JLabel();
		label.setFont(new Font("Verdana", Font.PLAIN, 18));
		label.setText(labelText);
		slider.setMajorTickSpacing(50);
		slider.setMinorTickSpacing(10);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		panel.setLayout(new GridLayout(number,1));
		panel.add(label);
		panel.add(slider);
		return slider;
	}
	
	
	/**
	 * gets the current Jframe
	 * @return
	 */
	public JFrame getFrame() {
		return frame;}
	
	 
	/**
	 * resizes the buffered image to 500x900
	 * @param BufferedImage 500x900
	 * @return
	 */
	 static BufferedImage resize(BufferedImage img, int widthX, int heightY) {
	        Image tmp = img.getScaledInstance(widthX, heightY, Image.SCALE_SMOOTH);
	        BufferedImage resized = new BufferedImage(widthX, heightY, BufferedImage.TYPE_INT_ARGB);
	        Graphics2D g2d = resized.createGraphics();
	        g2d.drawImage(tmp, 0, 0, null);
	        g2d.dispose();
	        return resized;
	    }
	
	

	 /**
	  * Chooses the correct new JFrame based on what button the user press. Uses actionevent and reads the 
	  * String content from the button
	  * @param e
	  * @param frame
	  * @throws ClassNotFoundException
	  * @throws InstantiationException
	  * @throws IllegalAccessException
	  * @throws UnsupportedLookAndFeelException
	  */
	public static void getNextFrame(ActionEvent e, JFrame frame) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {

		if(e.getActionCommand().equals("Main Menu")) {
			MainMenuFrame.mainMenuFrame();	
			frame.setVisible(false);
		}
		if(e.getActionCommand().equals("Background")) {			
			BackgroundFrame.backgroundFrame();
			frame.setVisible(false);	
		}
		if(e.getActionCommand().equals("Creature")) {
			CreatureFrame.creatureFrame();
			frame.setVisible(false);
		}
		if(e.getActionCommand().equals("Mouth")) {
			MouthFrame.mouthFrame();
			frame.setVisible(false);
		}
		if(e.getActionCommand().equals("Eyes")) {
			EyesFrame.eyesFrame();
			frame.setVisible(false);
		}
		if(e.getActionCommand().equals("Hair")) {
			HairFrame.hairFrame();		
			frame.setVisible(false);
		}
		if(e.getActionCommand().equals("Eyebrows")) {
			EyebrowsFrame.eyebrowsFrame();
			frame.setVisible(false);
		}
		if(e.getActionCommand().equals("Assesories")) {
			AssesoriesFrame.assesoriesFrame();		
			frame.setVisible(false);
		}
		if(e.getActionCommand().equals("Generate")) {
			Generate.generate();
		}
		if(e.getActionCommand().equals("Go Back")) {
			MainMenuFrame.mainMenuFrame();	
			frame.setVisible(false);
		}
		
		if(e.getActionCommand().equals("Preview Background")) {
			BufferedImage img = BackgroundFrame.getBackgrounds();
			frame.add(drawing(frame, (img), "BackgroundRefresh", 0, 800, 200, 500));
			frame.add(drawingMeta(frame, (img), "BackgroundRefresh"));

		}
		if(e.getActionCommand().equals("Preview Creature")) {
			BufferedImage img = CreatureFrame.getRandomCreature();
			frame.add(drawing(frame, resize(img, 500, 900), "BackgroundRefresh", 0, 800, 200, 500));
			frame.add(drawingMeta(frame, (img), "BackgroundRefresh"));
		}
		if(e.getActionCommand().equals("Preview Mouth")) {
			BufferedImage img = MouthFrame.getMouth();
			frame.add(drawing(frame, (img), "BackgroundRefresh", -300, 1100, 200, 500)); 
			frame.add(drawingMeta(frame, (img), "BackgroundRefresh"));
		}
		if(e.getActionCommand().equals("Preview Eyes")) {
			BufferedImage img = EyesFrame.getEyes();
			frame.add(drawing(frame, (img), "BackgroundRefresh", 0, 800, 200, 500));
			frame.add(drawingMeta(frame, (img), "BackgroundRefresh"));
		}
		if(e.getActionCommand().equals("Preview Hair")) {
			BufferedImage img = HairFrame.getHair();
			frame.add(drawing(frame, (img), "BackgroundRefresh", 0, 1200, 200, 500));
			frame.add(drawingMeta(frame, (img), "BackgroundRefresh"));
		}
		if(e.getActionCommand().equals("Preview Eyebrows")) {
			BufferedImage img = EyebrowsFrame.getEyebrows();
			frame.add(drawing(frame, (img), "BackgroundRefresh", 0, 800, 200, 500));
			frame.add(drawingMeta(frame, (img), "BackgroundRefresh"));
		}
		if(e.getActionCommand().equals("Preview Assesories")) {
			BufferedImage img = AssesoriesFrame.assesories();
			frame.add(drawing(frame, (img), "BackgroundRefresh", 0, 800, 200, 500));
			frame.add(drawingMeta(frame, (img), "BackgroundRefresh"));
		}
	
	}
	
	
	
	/**
	 * Makes a JButton with metadata-text corresponding to the trait
	 * variations. need to specify what JFrame it should be placed on
	 * needs the correct metadata-text and can specify placement up/down
	 * placementX is the x coordinate
	 * placementY is the y coordinate
	 * 
	 * @param frame
	 * @param image
	 * @param buttonText
	 * @param UpDown
	 * @param heigth
	 * @param placementX
	 * @param placementY
	 * @return
	 */
	public static JButton drawing(JFrame frame, BufferedImage image, String buttonText, int UpDown, int heigth, int placementX, int placementY) {
    	ImageIcon picture = new ImageIcon();
		JLabel label = new JLabel();
		picture.setImage(image);
		label.setIcon(picture);
		JButton buttonBig =new ButtonGenerator(buttonText, frame, placementX, UpDown, placementY, heigth).getButton();
		buttonBig.setIcon(picture);
		return buttonBig;
    }


	



	

	
	

}
