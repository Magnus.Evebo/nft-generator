package Frames;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.UnsupportedLookAndFeelException;

public class HairFrame {
	
	public static BufferedImage bufferedImage = new BufferedImage(780, 1688, BufferedImage.TYPE_INT_RGB);
    static Graphics2D g2d = bufferedImage.createGraphics();
	
    /**
     * Generates a new hairFrame with JButtons
     * 
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws UnsupportedLookAndFeelException
     */
	public static void hairFrame() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		JFrame hairFrame = new FrameGenerator("Hair").getFrame();
		new ButtonGenerator("Main Menu", hairFrame, 0,0);
		new ButtonGenerator("Preview Hair", hairFrame, 0,400);
		new ButtonGenerator("Go Back", hairFrame, 0,600).getButton().setForeground(new Color(46, 116, 43));
		
	}
	
	public static String metadataHair;
	
	/**
	 * gets a random BufferedImage hair and updates to the correct metatdata name
	 * @return BufferedImage hair
	 */
	public static BufferedImage getHair() {
		
			//colors for the hair
	        Color brownHair = new Color(103, 47, 21);
	        Color darkBlonde = new Color(156, 120, 63);
	        Color darkBrownHair = new Color(78, 22, 25);
	        Color blonde = new Color(239, 199, 86);
	        Color greenJoker = new Color(46, 125, 50);
	        Color purpleAlien = new Color(133, 77, 220);
	        
	        Random random = new Random();
	        List<Color> hairColor = Arrays.asList(brownHair, darkBlonde, darkBrownHair, blonde, greenJoker, purpleAlien);
	        Color randomHairColor = hairColor.get(random.nextInt(hairColor.size()));
	        int randomHairNumber = random.nextInt(4)+1;
	        g2d.setColor(Color.black);
	        g2d.fillRect(0, 0, 780, 1688);
	        if(randomHairNumber==1) {
	        	metadataHair = "Zigg zagg";
	            for(int i = 80; i < 250; i=i+20) {
	            	g2d.setColor(randomHairColor);
	                g2d.fillArc(100+i, 250+i, 350, 80, 10, 40);
	                g2d.fillArc(200-i, 250+i, 450, 80, 10, 110); 
	            }
	        }
	        else if(randomHairNumber==2) {
	        	metadataHair = "Curly";
	            for(int i = 80; i < 250; i=i+30) {
	            	g2d.setColor(randomHairColor);
	            	g2d.fillArc(10+(i/2), 200+i, 660, 110, 210, 110);
	                g2d.fillArc(200-i, 250+2*i, 350, 100, 80, 130);
	                g2d.fillArc(200+i, 200+2*i, 350, 110, 250, 160);
	            }
	        }
	        else if(randomHairNumber==3) {
	        	metadataHair = "Pherb";
	            for(int i = 20; i < 100; i=i+10) {
	            	g2d.setColor(randomHairColor);
	            	g2d.fillArc(150, 320, 850, 90, 100, 100);
	                g2d.fillArc(200-i, 250+2*i, 350, 100, 80, 130);
	                g2d.fillArc(200+i, 200+2*i, 350, 110, 250, 160);
	            }
	        }
	        else if(randomHairNumber==4) {
	        	metadataHair = "Jimmy";
	            for(int i = 20; i < 120; i=i+25) {
	            	g2d.setColor(randomHairColor);
	            	g2d.fillArc(150+i, 460-i*2, 350+i, 160, 100, 100+i*10);
	                g2d.fillArc(200-i, 250+2*i, 350, 100, 80, 130);
	                g2d.fillArc(70-i, 450+2*i, 350, 100, 80, 130);
	                g2d.fillArc(200+i, 200+2*i, 350, 110, 250, 160);
	                g2d.fillArc(330+i, 380+2*i, 350, 110, 250, 160);
	            }
	        }
	        g2d.setColor(Color.white);
	        Font font = new Font("Serif", Font.BOLD, 20);
	        g2d.setFont(font);
	        g2d.drawString(metadataHair, 360, 850);
			return bufferedImage;

	}

}
