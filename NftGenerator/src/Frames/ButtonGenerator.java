package Frames;


import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.UnsupportedLookAndFeelException;

public class ButtonGenerator {
	
	JButton button;
	FrameGenerator frameGenerator;
	
	/*
	 * creates a button that is 200x200, location on the JFrame needs to be specified
	 */
	public ButtonGenerator(String buttonText, JFrame frame, int locationX, int locationY) {
		this.button = new JButton(buttonText);
		button.setSize(200, 200);
		frame.add(button);
		button.setLocation(locationX, locationY);
		//actionlistener for the buttons
		button.addActionListener((ActionEvent e) ->{
		try {
			frameGenerator.getNextFrame(e, frame);
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InstantiationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnsupportedLookAndFeelException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		});	
	}
	
	/**
	 * generates a button on the given JFrame and size and location needs to be specified
	 * @param buttonText the text on the button
	 * @param frame 
	 * @param locationX x-coordinate
	 * @param locationY y-coordinate
	 * @param sizeX button width
	 * @param sizeY button height
	 */
	public ButtonGenerator(String buttonText, JFrame frame, int locationX, int locationY, int sizeX, int sizeY) {
		this.button = new JButton(buttonText);
		button.setSize(sizeX, sizeY);
		frame.add(button);
		button.setLocation(locationX, locationY);
		//actionlistener for the buttons
		button.addActionListener((ActionEvent e) ->{
		try {
			frameGenerator.getNextFrame(e, frame);
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InstantiationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnsupportedLookAndFeelException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		});	
	}
	
	
	
	/**
	 * gets button
	 * @return JButton button
	 */
	public JButton getButton() {
		return button;}	
}
