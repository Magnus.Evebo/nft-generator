package Frames;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.UnsupportedLookAndFeelException;

import traits.RainbowColors;

public class CreatureFrame {
	
	public static BufferedImage bufferedImage = new BufferedImage(780, 1688, BufferedImage.TYPE_INT_RGB);
    static Graphics2D g2d = bufferedImage.createGraphics();
	
    /**
     * generates 
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws UnsupportedLookAndFeelException
     */
	public static void creatureFrame() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		JFrame creatureFrame = new FrameGenerator("Creature").getFrame();
		new ButtonGenerator("Main Menu", creatureFrame, 0,0);
		new ButtonGenerator("Preview Creature", creatureFrame, 0,400);
		new ButtonGenerator("Go Back", creatureFrame, 0,600).getButton().setForeground(new Color(46, 116, 43));
		
	}
	
	/**
	 * generates a human creature body with corresponding metadata
	 * @return human BufferedImage
	 */
	  public static BufferedImage bodyHuman() {
		  	String metadataBody = "Human";
	        //omriss
	        g2d.setColor(Color.black);
	        g2d.fillRect(0, 0, 780, 1688);
	        g2d.fillRect(337, 1340, 106, 106);
	        //hand omriss
	        g2d.fillRect(192, 1560-20, 91, 41);
	        g2d.fillRect(502, 1560-20, 91, 41);
	        
	    	List<Color> skinColorList = RainbowColors.getRandomSkin(RainbowColors.allSkin);
			Color skinTone = RainbowColors.getSkinTone(skinColorList);
			Color skinContrast = RainbowColors.getSkinContrast(skinColorList);

	    	//neck
	        g2d.setColor(skinTone);
	        g2d.fillRect(340, 1340, 100, 100);
	        
	        g2d.setColor(Color.black);
	        g2d.fillOval(63, 342, 654, 1004);
	        //head
	    	g2d.setColor(skinTone);
	        g2d.fillOval(65, 344, 650, 1000);
	        //hand
	        g2d.fillRect(195, 1563-20, 85, 35);
	        g2d.fillRect(505, 1563-20, 85, 35);
	      //ear
	        g2d.setColor(Color.black);
	        g2d.fillOval(35-2, 697, 64, 236);
	        g2d.fillOval(685-1, 697, 64, 236);
	        //nose
	        g2d.setColor(skinContrast);
	        g2d.fillArc(340, 950, 100, 100, 135, 270);
	        g2d.fillArc(300, 700, 180, 300, 253, 35);
	      //ear
	        g2d.fillOval(35, 700, 60, 230);
	        g2d.fillOval(685, 700, 60, 230);
	        //ear-cross
	        g2d.setColor(skinTone);
	        //right
	        g2d.drawLine(715, 705, 715, 925);
	        g2d.drawLine(690, 810, 740, 810);
	        //left
	        g2d.drawLine(65, 705, 65, 925);
	        g2d.drawLine(40, 810, 90, 810);
	        
	        g2d.setColor(Color.black);
	          Font font = new Font("Serif", Font.BOLD, 20);
	          g2d.setFont(font);
	          g2d.drawString(metadataBody, 360, 850);
	        
			return bufferedImage;
	    }
	  
	  /**
	   * generates a alien creature body with corresponding metadata
	   * @return alien BufferedImage
	   */
	  public static BufferedImage bodyAlien() {
		  String metadataBody = "Alien";
      	//hals
          g2d.setColor(new Color(108, 196, 23));
          g2d.fillRect(340, 1340, 100, 100);
          
          g2d.setColor(Color.black);
          g2d.fillOval(63, 342, 654, 1004);
          //hode
      	  g2d.setColor(new Color(108, 196, 23));
          g2d.fillOval(65, 344, 650, 1000);
          g2d.fillRect(195, 1563-20, 85, 35);
          g2d.fillRect(505, 1563-20, 85, 35);
        //�re omriss
          g2d.setColor(Color.black);
          g2d.fillOval(35-2, 697, 64, 236);
	        g2d.fillOval(685-1, 697, 64, 236);
        //nase
          g2d.setColor(new Color(108+20, 196+20, 23+20));
          g2d.fillArc(340, 950, 100, 100, 135, 270);
          g2d.fillArc(300, 700, 180, 300, 253, 35);
          //�re
          g2d.fillOval(35, 700, 60, 230);
          g2d.fillOval(685, 700, 60, 230);
          //�yre kryss
          g2d.setColor(new Color(153+20,50+20,204+20));
          //h�gre
          g2d.drawLine(715, 705, 715, 925);
          g2d.drawLine(690, 810, 740, 810);
          //venstre
          g2d.drawLine(65, 705, 65, 925);
          g2d.drawLine(40, 810, 90, 810);
          
          g2d.setColor(Color.black);
          Font font = new Font("Serif", Font.BOLD, 20);
          g2d.setFont(font);
          g2d.drawString(metadataBody, 360, 850);
          
          //antenna
          g2d.setColor(Color.black);
          g2d.fillOval(150, 120, 80, 80);
          g2d.fillOval(550, 120, 80, 80);
          for(int i=0; i<160; i=i+8) {
              g2d.fillRect(328-i, 447-(i*2), 27, 27);
              g2d.fillRect(430+i, 447-(i*2), 27, 27);
          }
          g2d.setColor(new Color(108+30, 196+30, 23+30));
          g2d.fillOval(155, 125, 70, 70);
          g2d.fillOval(555, 125, 70, 70);
          for(int i=0; i<160; i=i+8) {
              g2d.fillRect(330-i, 450-(i*2), 25, 25);
              g2d.fillRect(430+i, 450-(i*2), 25, 25);
          }
          g2d.setColor(new Color(153,50,204));
          g2d.fillOval(170, 140, 40, 40);
          g2d.fillOval(570, 140, 40, 40);
 
          return bufferedImage;
      }
	  
	  /**
	   * generates a zombie creature body with corresponding metadata
	   * @return zombie BufferedImage
	   */
	  public static BufferedImage bodyZombie() {
      	String metadataBody = "Zombie";
      	//neck
      	  g2d.setColor(Color.black);
      	  g2d.fillRect(0, 0, 780, 1688);
          g2d.setColor(new Color(83, 154, 119));
          g2d.fillRect(340, 1340, 100, 100);
          
          g2d.setColor(Color.black);
          g2d.fillOval(63, 342, 654, 1004);
          //head
      	g2d.setColor(new Color(83, 154, 119));
          g2d.fillOval(65, 344, 650, 1000);
          g2d.fillRect(195, 1563-20, 85, 35);
          g2d.fillRect(505, 1563-20, 85, 35);
        //ear
          g2d.setColor(Color.black);
          g2d.fillOval(35-2, 697, 64, 236);
	        g2d.fillOval(685-1, 697, 64, 236);
          //nose
          g2d.setColor(new Color(83-20, 154-20, 119-20));
          g2d.fillArc(340, 950, 100, 100, 135, 270);
        //ear
          g2d.fillOval(35, 700, 60, 230);
          g2d.fillOval(685, 700, 60, 230);
          g2d.setColor(new Color(209, 0, 28));
          //right
          g2d.drawLine(715, 705, 715, 925);
          g2d.drawLine(690, 810, 740, 810);
          //left
          g2d.drawLine(65, 705, 65, 925);
          g2d.drawLine(40, 810, 90, 810);
          //dotts
          g2d.setColor(new Color(96, 141, 139));
          g2d.fillOval(300, 400, 50, 60);
          g2d.fillOval(320, 1050, 50, 60);
          g2d.fillOval(620, 750, 50, 60);
          g2d.fillOval(220, 750, 50, 30);
          g2d.fillOval(320, 550, 50, 50);
          g2d.fillOval(320, 1200, 50, 50);
          g2d.fillOval(520, 950, 50, 50);
          g2d.fillOval(460, 1250, 50, 50);
          g2d.fillOval(250, 1150, 50, 50);
          g2d.fillOval(200, 850, 70, 80);
          g2d.fillOval(400, 650, 70, 80);
          g2d.fillOval(550, 500, 70, 80);
          g2d.fillOval(590, 1100, 30, 40);
          
          g2d.setColor(new Color(209, 0, 28));
          g2d.drawLine(130, 950-8, 300, 1100-8);
          g2d.drawLine(130, 950-4, 300, 1100-4);
          g2d.drawLine(130, 950, 300, 1100);
          g2d.drawLine(130, 952, 300, 1102);
          g2d.drawLine(130, 956, 300, 1106);
          g2d.drawLine(130, 959, 300, 1109);
          
          g2d.drawLine(180, 959, 280, 1109);
          g2d.drawLine(180, 959+4, 280, 1109+4);
          g2d.drawLine(180, 959+8, 280, 1109+8);
          g2d.drawLine(180, 959+12, 280, 1109+12);
          
          g2d.setColor(new Color(218, 51, 73));
          g2d.fillOval(230, 1050, 6, 20);
          g2d.fillOval(240, 1080, 6, 20);
          g2d.fillOval(200, 1020, 6, 20);
          g2d.fillOval(215, 1060, 6, 20);
          g2d.fillOval(225, 1100, 6, 20);
 
          g2d.setColor(Color.black);
          Font font = new Font("Serif", Font.BOLD, 20);
          g2d.setFont(font);
          g2d.drawString(metadataBody, 360, 850);
		return bufferedImage;
	}
	  
	  /**
	   * generates a random creature BufferedImage
	   * @return BufferedImage
	   */
	  public static BufferedImage getRandomCreature() {
	    	Random random = new Random();
	    	int ranNum = random.nextInt(3);
	    	if(ranNum==0) {
	    		return bodyZombie();
	    	}
	    	if(ranNum==1) {
	    		return bodyAlien();
	    	}
	    	else {
	    		return bodyHuman();
	    	}	
	    }

}
