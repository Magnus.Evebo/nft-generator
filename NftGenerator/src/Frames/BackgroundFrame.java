package Frames;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UnsupportedLookAndFeelException;


public class BackgroundFrame extends JFrame{

	static int width = 780;
    static int height = 1688;
    public static BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    static Graphics2D g2d = bufferedImage.createGraphics();
    
	
    /**
     * creates the background JFrame with buttons
     * 
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws UnsupportedLookAndFeelException
     */
	public static void backgroundFrame() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		JFrame backgroundFrame = new FrameGenerator("Background").getFrame();
		JButton button = new ButtonGenerator("Main Menu", backgroundFrame, 0,0).getButton();
		new ButtonGenerator("Preview Background", backgroundFrame, 0,400);
		new ButtonGenerator("Go Back", backgroundFrame, 0,600).getButton().setForeground(new Color(46, 116, 43));
	}



	public static String metadataBackground = "";

	//@Override
	/**
	 * gets a random BufferedImage background and updates to the correct metatdata name 
	 * @return background BufferedImage
	 */
	public static BufferedImage getBackgrounds() {
		Random random = new Random();
		
		int randomBackgroundNumber = random.nextInt(5)+1;
        int R = random.nextInt(240);
        int G = random.nextInt(240);
        int B = random.nextInt(240);
        int R2 = random.nextInt(240);
        int G2 = random.nextInt(240);
        int B2 = random.nextInt(240);
        //solid background with small stripe rectangles 		
        if(randomBackgroundNumber==1) {
        	metadataBackground = "Rectangle dots"; 
        	g2d.setColor(new Color (R, G, B));
            g2d.fillRect(0, 0, width, height);
        	for(int i = 0; i < 780; i=i+82) {
            	for(int j = 0; j < 1688; j=j+206) {
            		g2d.setColor(new Color (B, R, G));
                    g2d.fillRect(i, j, 42, 42);
            	} 
            }
        }
        else if(randomBackgroundNumber==2) {
        	metadataBackground = "Sirkles";
        	g2d.setColor(new Color(B, R, G));
            g2d.fillRect(0, 0, width, height);
        	for(int i = 0; i < 780; i=i+200) {
            	for(int j = 0; j < 1688; j=j+251) {
            		g2d.setColor(new Color(R, B, G));
                    g2d.fillOval(i, j, 179, 179);
            	} 
            }
        } 
        else if(randomBackgroundNumber==3) {
        	metadataBackground = "Squares";
        	g2d.setColor(new Color(R, B, G));
            g2d.fillRect(0, 0, width, height);
        	g2d.setColor(new Color(B, R, G));
        	g2d.fillRect(0, 0, 390, 844);
        	g2d.setColor(new Color(B, R, G));
        	g2d.fillRect(390, 844, 390, 844);
        }
        else if(randomBackgroundNumber==4) {
        	metadataBackground = "Pattern";
        	g2d.setColor(new Color (B, R, G));
            g2d.fillRect(0, 0, width, height);
        	g2d.setColor(new Color(R, G, B));
        	g2d.fillRect(0, 0, 390, 844);
        	g2d.fillRect(390, 844, 390, 844);
        	for(int i = 0; i < 780; i=i+200) {
            	for(int j = 0; j < 1688; j=j+251) {
            		g2d.setColor(new Color(B2, R2, G2));
                    g2d.fillOval(i, j, 179, 179);
            	}
            }
        	for(int i = 0; i < 780; i=i+82) {
            	for(int j = 0; j < 1688; j=j+274) {
            		g2d.setColor(new Color(R2, G2, B2));
                    g2d.fillRect(i, j, 42, 42);
                    
            	} 
            }
        }
        else if(randomBackgroundNumber==5) {
        	metadataBackground = "Static";
        	g2d.setColor(new Color(R, G, B));
            g2d.fillRect(0, 0, width, height);
            
        }
        g2d.setColor(Color.black);
        Font font = new Font("Serif", Font.BOLD, 20);
        g2d.setFont(font);
        g2d.drawString(metadataBackground, 360, 850);
		return bufferedImage;	
	}
    
	
  

	

}
