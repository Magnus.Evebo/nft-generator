package Frames;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.UnsupportedLookAndFeelException;

import traits.RainbowColors;

public class AssesoriesFrame {
	
	
    static Color red = new Color(211, 47, 47);
    static Color yellow = new Color(255, 255, 0);
    static Color brown = new Color(145, 122, 116);
    static Color green = new Color(61, 158, 11);
    static Color lightBlue = new Color(90, 138, 211);
    static Color blue = new Color(83, 136, 255);
    static Color deepred = new Color(191, 10, 48);
    static Color lightGreen = new Color(76, 175, 80);
    static Color purple = new Color(149, 20, 223);
    Color glassDarkGrey = new Color(58, 58, 58);
	Color glassGrey = new Color(210, 209, 204);
	Color lightGrey = new Color(134, 134, 134);
	Color lightBlack = new Color(38, 38, 38);
	Color darkGold = new Color (183, 153, 6);
	Color lightGold = new Color (249, 224, 159);
	Color lightGreenBlue = new Color (57, 240, 119);
	Color lightBlueGreen = new Color (0, 201, 167);
	

    public static BufferedImage bufferedImage = new BufferedImage(780, 1688, BufferedImage.TYPE_INT_RGB);
    static Graphics2D g2d = bufferedImage.createGraphics();
	
    /**
     * creates the assesories JFrame with JButtons
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws UnsupportedLookAndFeelException
     */
	static void assesoriesFrame() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		JFrame assesoriesFrame = new FrameGenerator("Assesories").getFrame();
		new ButtonGenerator("Main Menu", assesoriesFrame, 0,0);
		new ButtonGenerator("Preview Assesories", assesoriesFrame, 0,400);
		new ButtonGenerator("Go Back", assesoriesFrame, 0,600).getButton().setForeground(new Color(46, 116, 43));
		
	}
	
	
	
	
	public static String metadataAssesories = "";
	
	/**
	 * draws a random assesories BufferedImage with the corresponding metadata text
	 * @return random assesories BufferImage
	 */
	public static BufferedImage assesories() {
		//jordan 1
		Random random = new Random();
        List<Color> shoeColor = Arrays.asList(yellow, brown, red, green, lightBlue, blue, deepred, lightGreen, purple);
        Color randomShoeColor = shoeColor.get(random.nextInt(shoeColor.size()));
        
       int randomAssesoriesNumber = random.nextInt(2)+1;
        int rightside = -50; 
        int leftside = -250;
        int updown = -600;
        g2d.setColor(Color.GRAY.brighter());
        g2d.fillRect(0, 0, 780, 1688);
        if(randomAssesoriesNumber == 1) {
        	metadataAssesories = "Jordans";
        	
            g2d.setColor(Color.white);
            //g2d.fillRect(0, 0, 780, 1688);
            g2d.fillRect(500+rightside, 1645+updown, 120, 5);
            g2d.fillRect(610+rightside, 1640+updown, 25, 5);
            g2d.fillRect(630+rightside, 1635+updown, 15, 5);
            g2d.fillRect(500+rightside, 1640+updown, 25, 5);
            g2d.fillRect(545+rightside, 1640+updown, 25, 5);
            g2d.fillRect(540+rightside, 1635+updown, 30, 5);
            g2d.fillRect(500+rightside, 1600+updown, 15, 5);
            g2d.fillRect(510+rightside, 1605+updown, 55, 5);
            g2d.fillRect(520+rightside, 1610+updown, 35, 5);
            g2d.fillRect(520+rightside, 1615+updown, 40, 5);
            g2d.fillRect(565+rightside, 1630+updown, 10, 5);
            g2d.fillRect(570+rightside, 1625+updown, 10, 5);
            g2d.fillRect(570+rightside, 1620+updown, 10, 5);
            g2d.fillRect(540+rightside, 1620+updown, 15, 5);
            g2d.fillRect(535+rightside, 1630+updown, 10, 5);
            g2d.fillRect(560+rightside, 1610+updown, 10, 5);
            g2d.fillRect(565+rightside, 1615+updown, 10, 5);
            g2d.fillRect(540+rightside, 1600+updown, 10, 5);
            g2d.setColor(Color.black);
            g2d.fillRect(570+rightside, 1640+updown, 40, 5);
            g2d.fillRect(570+rightside, 1635+updown, 65, 5);
            g2d.fillRect(575+rightside, 1630+updown, 25, 5);
            g2d.fillRect(580+rightside, 1625+updown, 20, 5);
            g2d.fillRect(580+rightside, 1620+updown, 20, 5);
            g2d.fillRect(575+rightside, 1615+updown, 20, 5);
            g2d.fillRect(570+rightside, 1610+updown, 15, 5);
            g2d.fillRect(565+rightside, 1605+updown, 15, 5);
            g2d.fillRect(550+rightside, 1600+updown, 20, 5);
            g2d.fillRect(525+rightside, 1600+updown, 15, 5);
            g2d.fillRect(535+rightside, 1595+updown, 25, 5);
            g2d.fillRect(545+rightside, 1590+updown, 10, 5);
            g2d.fillRect(545+rightside, 1585+updown, 5, 5);
            g2d.fillRect(545+rightside, 1580+updown, 5, 5);
            g2d.fillRect(570+rightside, 1640+updown, 40, 5);
            g2d.fillRect(640+rightside, 1625+updown, 5, 5);
            g2d.fillRect(605+rightside, 1630+updown, 40, 5);
            g2d.fillRect(600+rightside, 1620+updown, 5, 5);
            g2d.fillRect(520+rightside, 1575+updown, 20, 5);
            g2d.fillRect(510+rightside, 1580+updown, 15, 5);
            g2d.fillRect(505+rightside, 1585+updown, 10, 5);
            //swoosh
            g2d.setColor(Color.black);
            g2d.fillRect(545+rightside, 1630+updown, 20, 5);
            g2d.fillRect(530+rightside, 1625+updown, 40, 5);
            g2d.fillRect(525+rightside, 1620+updown, 15, 5);
            g2d.fillRect(515+rightside, 1615+updown, 15, 5);
            g2d.fillRect(510+rightside, 1610+updown, 15, 5);
            g2d.fillRect(500+rightside, 1605+updown, 10, 5);
            g2d.fillRect(555+rightside, 1620+updown, 15, 5);
            g2d.fillRect(555+rightside, 1615+updown, 10, 5);
            g2d.fillRect(555+rightside, 1610+updown, 5, 5);
            //toe
            g2d.setColor(randomShoeColor);
            g2d.fillRect(600+rightside, 1630+updown, 5, 5);
            g2d.fillRect(600+rightside, 1625+updown, 40, 5);
            g2d.fillRect(605+rightside, 1620+updown, 20, 5);
            //high topp
            g2d.fillRect(500+rightside, 1595+updown, 35, 5);
            g2d.fillRect(515+rightside, 1600+updown, 10, 5);
            g2d.fillRect(505+rightside, 1590+updown, 40, 5);
            g2d.fillRect(515+rightside, 1585+updown, 30, 5);
            g2d.fillRect(525+rightside, 1580+updown, 20, 5);
            g2d.fillRect(540+rightside, 1575+updown, 5, 5);
            //s�lefarge
            g2d.fillRect(500+rightside, 1650+updown, 120, 5);
            g2d.fillRect(620+rightside, 1645+updown, 15, 5);
            g2d.fillRect(635+rightside, 1640+updown, 10, 5);
            //skofarge hel bak
            g2d.fillRect(525+rightside, 1640+updown, 20, 5);
            g2d.fillRect(500+rightside, 1635+updown, 40, 5);
            g2d.fillRect(500+rightside, 1630+updown, 35, 5);
            g2d.fillRect(500+rightside, 1625+updown, 35, 5);
            g2d.fillRect(500+rightside, 1620+updown, 30, 5);
            g2d.fillRect(500+rightside, 1615+updown, 25, 5);
            g2d.fillRect(500+rightside, 1610+updown, 10, 5);
        
       
        	g2d.setColor(Color.white);
            g2d.fillRect(500+leftside, 1645+updown, 120, 5);
            g2d.fillRect(610+leftside, 1640+updown, 25, 5);
            g2d.fillRect(630+leftside, 1635+updown, 15, 5);
            g2d.fillRect(500+leftside, 1640+updown, 25, 5);
            g2d.fillRect(545+leftside, 1640+updown, 25, 5);
            g2d.fillRect(540+leftside, 1635+updown, 30, 5);
            g2d.fillRect(500+leftside, 1600+updown, 15, 5);
            g2d.fillRect(510+leftside, 1605+updown, 55, 5);
            g2d.fillRect(520+leftside, 1610+updown, 35, 5);
            g2d.fillRect(520+leftside, 1615+updown, 40, 5);
            g2d.fillRect(565+leftside, 1630+updown, 10, 5);
            g2d.fillRect(570+leftside, 1625+updown, 10, 5);
            g2d.fillRect(570+leftside, 1620+updown, 10, 5);
            g2d.fillRect(540+leftside, 1620+updown, 15, 5);
            g2d.fillRect(535+leftside, 1630+updown, 10, 5);
            g2d.fillRect(560+leftside, 1610+updown, 10, 5);
            g2d.fillRect(565+leftside, 1615+updown, 10, 5);
            g2d.fillRect(540+leftside, 1600+updown, 10, 5);
            //g2d.fillRect(535, 1630, 40, 5);
            g2d.setColor(Color.black);
            g2d.fillRect(570+leftside, 1640+updown, 40, 5);
            g2d.fillRect(570+leftside, 1635+updown, 65, 5);
            g2d.fillRect(575+leftside, 1630+updown, 25, 5);
            g2d.fillRect(580+leftside, 1625+updown, 20, 5);
            g2d.fillRect(580+leftside, 1620+updown, 20, 5);
            g2d.fillRect(575+leftside, 1615+updown, 20, 5);
            g2d.fillRect(570+leftside, 1610+updown, 15, 5);
            g2d.fillRect(565+leftside, 1605+updown, 15, 5);
            g2d.fillRect(550+leftside, 1600+updown, 20, 5);
            g2d.fillRect(525+leftside, 1600+updown, 15, 5);
            g2d.fillRect(535+leftside, 1595+updown, 25, 5);
            g2d.fillRect(545+leftside, 1590+updown, 10, 5);
            g2d.fillRect(545+leftside, 1585+updown, 5, 5);
            g2d.fillRect(545+leftside, 1580+updown, 5, 5);
            g2d.fillRect(570+leftside, 1640+updown, 40, 5);
            g2d.fillRect(640+leftside, 1625+updown, 5, 5);
            g2d.fillRect(605+leftside, 1630+updown, 40, 5);
            g2d.fillRect(600+leftside, 1620+updown, 5, 5);
            g2d.fillRect(520+leftside, 1575+updown, 20, 5);
            g2d.fillRect(510+leftside, 1580+updown, 15, 5);
            g2d.fillRect(505+leftside, 1585+updown, 10, 5);
            //swoosh
            g2d.setColor(Color.black);
            g2d.fillRect(545+leftside, 1630+updown, 20, 5);
            g2d.fillRect(530+leftside, 1625+updown, 40, 5);
            g2d.fillRect(525+leftside, 1620+updown, 15, 5);
            g2d.fillRect(515+leftside, 1615+updown, 15, 5);
            g2d.fillRect(510+leftside, 1610+updown, 15, 5);
            g2d.fillRect(500+leftside, 1605+updown, 10, 5);
            g2d.fillRect(555+leftside, 1620+updown, 15, 5);
            g2d.fillRect(555+leftside, 1615+updown, 10, 5);
            g2d.fillRect(555+leftside, 1610+updown, 5, 5);
            //toe
            g2d.setColor(randomShoeColor);
            g2d.fillRect(600+leftside, 1630+updown, 5, 5);
            g2d.fillRect(600+leftside, 1625+updown, 40, 5);
            g2d.fillRect(605+leftside, 1620+updown, 20, 5);
            //high topp
            g2d.fillRect(500+leftside, 1595+updown, 35, 5);
            g2d.fillRect(515+leftside, 1600+updown, 10, 5);
            g2d.fillRect(505+leftside, 1590+updown, 40, 5);
            g2d.fillRect(515+leftside, 1585+updown, 30, 5);
            g2d.fillRect(525+leftside, 1580+updown, 20, 5);
            g2d.fillRect(540+leftside, 1575+updown, 5, 5);
            //s�lefarge
            g2d.fillRect(500+leftside, 1650+updown, 120, 5);
            g2d.fillRect(620+leftside, 1645+updown, 15, 5);
            g2d.fillRect(635+leftside, 1640+updown, 10, 5);
            //skofarge hel bak
            g2d.fillRect(525+leftside, 1640+updown, 20, 5);
            g2d.fillRect(500+leftside, 1635+updown, 40, 5);
            g2d.fillRect(500+leftside, 1630+updown, 35, 5);
            g2d.fillRect(500+leftside, 1625+updown, 35, 5);
            g2d.fillRect(500+leftside, 1620+updown, 30, 5);
            g2d.fillRect(500+leftside, 1615+updown, 25, 5);
            g2d.fillRect(500+leftside, 1610+updown, 10, 5);
        
            g2d.setColor(Color.black);
            Font font = new Font("Serif", Font.BOLD, 20);
            g2d.setFont(font);
    		g2d.drawString(metadataAssesories, 360, 850);
		return bufferedImage;
		}
        
        if(randomAssesoriesNumber == 2) {
        	Color randomGlassColor = RainbowColors.getRandomColor(RainbowColors.allColors);
            Color randomGlassesColor = RainbowColors.getRandomColor(RainbowColors.allColors);
            int randomGlassesNumber = random.nextInt(2)+1;
            
            if(randomGlassesNumber==1) {
            	metadataAssesories = "Normal Glasses";
            	for(int i = 200; i < 350; i=i+8) {
                	for(int j = 690; j < 880; j=j+8) {
                		g2d.setColor(randomGlassColor);
                        g2d.fillOval(i, j, 7, 7);
                	}
                }
            	for(int i = 440; i < 590; i=i+8) {
                	for(int j = 690; j < 880; j=j+8) {
                        g2d.fillOval(i, j, 7, 7);
                	}
                }
            	for(int i=0; i<160; i=i+1) {
                	g2d.setColor(randomGlassesColor);
                    g2d.fillRect(200+i, 680-(i/20), 15, 20);
                    g2d.fillRect(440+i, 680-(i/20), 15, 20);
                    g2d.fillRect(180+i, 880-(i/20), 15, 20);
                    g2d.fillRect(420+i, 880-(i/20), 15, 20);
                    g2d.fillRect(350+(i/2), 780+(i/20), 15, 20);
                }
            	for(int i=0; i<20; i=i+1) {
                    g2d.fillRect(180+i, 880-(i*10), 15, 20);
                    g2d.fillRect(420+i, 880-(i*10), 15, 20);
                    g2d.fillRect(340+i, 865-(i*10), 15, 20);
                    g2d.fillRect(580+i, 865-(i*10), 15, 20);
                }
            	for(int i=0; i<130; i=i+10) {
                    g2d.fillRect(190-i, 750-(i/3), 20, 25);
                    g2d.fillRect(590+i, 760-(i/3), 20, 25);
                }	
            //////////////////////////////////////	
            }
            
            else if(randomGlassesNumber==2) {
            	metadataAssesories = "Crucked Glasses";
            	//brilleglass venstre
            	for(int i = 215; i < 350; i=i+9) {
                	for(int j = 700; j < 880; j=j+8) {
                		g2d.setColor(randomGlassColor);
                        g2d.fillOval(i, j, 7, 7);
                	}
                }
            	//rigth 
            	for(int i = 420; i < 560; i=i+9) {
                	for(int j = 700; j < 880; j=j+8) {
                        g2d.fillOval(i, j, 7, 7);
                	}
                }
            	for(int i=0; i<100; i=i+1) {
                	g2d.setColor(randomGlassesColor);
                    g2d.fillRect(218+i, 690-(i/20), 15, 20);
                    g2d.fillRect(430+i, 680-(i/20), 15, 20);
                    g2d.fillRect(235+i, 880-(i/20), 15, 20);
                    g2d.fillRect(410+i, 880-(i/20), 15, 20);
                    g2d.fillRect(405-(i/2), 780+(i/20), 15, 20);
                }
            	for(int i=0; i<14; i=i+1) {
                    g2d.fillRect(190+i, 860-(i*10), 15, 20);
                    g2d.fillRect(400+i, 860-(i*10), 15, 20);
                    g2d.fillRect(340+i, 855-(i*10), 15, 20);
                    g2d.fillRect(560+i, 845-(i*10), 15, 20);
                }
            	for(int i=0; i<20; i=i+10) {
                    g2d.fillRect(340-i, 700-(i/2), 20, 25);
                    g2d.fillRect(420+i, 710-(i/2), 20, 25);
                }
            	for(int i=0; i<25; i=i+10) {
                    g2d.fillRect(220-i, 865-(i/2), 20, 25);
                    g2d.fillRect(520+i, 865-(i/2), 20, 25);
                }
                g2d.fillRect(215, 710, 20, 30);
                g2d.fillRect(555, 690, 20, 35);
                g2d.fillRect(535, 690, 20, 20);
                g2d.fillRect(423, 690, 20, 20);
                
                for(int i=0; i<130; i=i+10) {
                    g2d.fillRect(190-i, 750-(i/3), 20, 25);
                    g2d.fillRect(570+i, 750-(i/3), 20, 25);
                }       
            }
    		
        }
        g2d.setColor(Color.black);
        Font font = new Font("Serif", Font.BOLD, 20);
        g2d.setFont(font);
		g2d.drawString(metadataAssesories, 350, 930);
		
		return bufferedImage;
	
	
	
	}
	
	

}
