package traits;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

public class MenuBar {

	public static void MenuBar(JFrame frame) {
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		JMenu background = new JMenu("Background");
		menuBar.add(background);
		JMenu creature = new JMenu("Creature");
		menuBar.add(creature);
		JMenu eyes = new JMenu("Eyes");
		menuBar.add(eyes);
		JMenu mouth = new JMenu("Mouth");
		menuBar.add(mouth);
		JMenu hair = new JMenu("Hair");
		menuBar.add(hair);
		JMenu eyebrows = new JMenu("Eyebrows");
		menuBar.add(eyebrows);
		JMenu assesories = new JMenu("Assesories");
		menuBar.add(assesories);
	
		
	}
}
