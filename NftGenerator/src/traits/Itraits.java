package traits;

import java.awt.image.BufferedImage;

public interface Itraits {
	
	/**
	 * Generates a new random background
	 * @return BufferedImage
	 */
	public BufferedImage background();
	
	/**
	 * Generates a new human, alien or zombie body
	 * @return BufferedImage
	 */
	public BufferedImage body();
	
	/**
	 * Generates new random eyes with random eyecolor
	 * @return BufferedImage
	 */
	public BufferedImage eyes();
	
	/**
	 * Generates a new random mouth with brown, white or gold teeth
	 * @return BufferedImage
	 */
	public BufferedImage mouth();
	
	/**
	 * Generates a new hairstyle with a random color
	 * @return BufferedImage
	 */
	public BufferedImage hair();
	
	/**
	 * Generates alien antennas if the body is alien
	 * @return BufferedImage
	 */
	public BufferedImage alienAntenna();
	
	/**
	 * Generates nose holes
	 * @return BufferedImage
	 */
	public BufferedImage nose();
	
	/**
	 * Generates random eyebrows
	 * @return BufferedImage
	 */
	public BufferedImage eyebrows();
	
	/**
	 * Generates glasses
	 * @return BufferedImage
	 */
	public BufferedImage glasses();
	
	/**
	 * Generates shirt, pants and boots in different random colors
	 * @return BufferedImage
	 */
	public BufferedImage clothes();
	
	/**
	 * Generates rare accessories
	 * @return BufferedImage
	 */
	public BufferedImage accessories();
	
	public static BufferedImage joinBufferedImage(BufferedImage background,
            BufferedImage creature,
            BufferedImage mouth,
            BufferedImage eyes,
            BufferedImage hair,
            BufferedImage alienAntenna,
            BufferedImage nose,
            BufferedImage eyebrows,
            BufferedImage glasses,
            BufferedImage clothes,
            BufferedImage accessories) {
		return null ;
	}
	
	

}