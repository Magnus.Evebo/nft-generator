package traits;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import Frames.FrameGenerator;


public class Traits implements Itraits{
	
	//////////////////// COLORS /////////////////////////////
	Color black = new Color(0, 0, 0);
    Color white = new Color(255, 255, 255);
    Color red = new Color(211, 47, 47);
    Color yellow = new Color(255, 255, 0);
    Color lightyellow = new Color(255, 255, 102);
    Color brown = new Color(145, 122, 116);
    Color green = new Color(61, 158, 11);
    Color lightBlue = new Color(90, 138, 211);
    Color blue = new Color(83, 136, 255);
    Color deepred = new Color(191, 10, 48);
    Color lightGreen = new Color(76, 175, 80);
    Color purple = new Color(149, 20, 223);
    Color glassDarkGrey = new Color(58, 58, 58);
	Color glassGrey = new Color(210, 209, 204);
	Color lightGrey = new Color(134, 134, 134);
	Color lightBlack = new Color(38, 38, 38);
	Color darkGold = new Color (183, 153, 6);
	Color lightGold = new Color (249, 224, 159);
	Color lightGreenBlue = new Color (57, 240, 119);
	Color lightBlueGreen = new Color (0, 201, 167);
////////////////////COLORS //////////////////////////////////
	
    
    /////////////////// METADATA ////////////////////////
    public String metadataBackground = "";
    public String metadataBody = "";
    public String metadataMouth = "";
    public String metadataEye = "";
    public String metadataHair = "";
    public String metadataEyebrows ="";
    public String metadataGlasses = "none";
    /////////////////// METADATA /////////////////////////
    
	
	
	
	
	
	//picture dimensions
	int width = 780;
    int height = 1688;
    Random random = new Random();
    public BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    Graphics2D g2d = bufferedImage.createGraphics();
	public boolean alien;


    // Constructs a BufferedImage of one of the predefined image types.
    public Traits(int width, int height) {
    	this.width = width;
    	this.height = height;
    }
    
    /**
     * gets the BufferedImage
     * @return BufferedImage
     */
    public BufferedImage getBufferedImage() {
		return bufferedImage;
    }
    

    public static BufferedImage bufferedImageBackground;
	
    /**
     * Generates a BufferedImage based on the input from the user
     * Values: Pattern, circles, squares, static, rectangles.
     * the default value is static
     * 
     * return background BufferedImage
     */
    public BufferedImage background() {
		int patternNumber = FrameGenerator.pattern;
		int circlesNumber = FrameGenerator.sircles;
		int squaresNumber = FrameGenerator.squares;
		int staticNumber = FrameGenerator.staticb;
		int rectanglesNumber = FrameGenerator.rectangles;
		
		int totalNumber = rectanglesNumber+circlesNumber+squaresNumber+patternNumber+staticNumber;
		
		//random.nextInt("can not be 0")
		if(totalNumber>0) {
			int randomBackgroundNumber = random.nextInt(totalNumber);
	        int R = random.nextInt(240);
	        int G = random.nextInt(240);
	        int B = random.nextInt(240);
	        int R2 = random.nextInt(240);
	        int G2 = random.nextInt(240);
	        int B2 = random.nextInt(240);
	        //solid background with small stripe rectangles 		
	        if(randomBackgroundNumber>=0 && randomBackgroundNumber<rectanglesNumber) {
	        	metadataBackground = "Rectangle dots"; 
	        	g2d.setColor(new Color (R, G, B));
	            g2d.fillRect(0, 0, width, height);
	        	for(int i = 0; i < 780; i=i+82) {
	            	for(int j = 0; j < 1688; j=j+206) {
	            		g2d.setColor(new Color (B, R, G));
	                    g2d.fillRect(i, j, 42, 42);
	            	} 
	            }
	        }
	        
	        else if(randomBackgroundNumber>=rectanglesNumber && randomBackgroundNumber<(rectanglesNumber+circlesNumber)) {
	        	metadataBackground = "Circles";
	        	g2d.setColor(new Color(B, R, G));
	            g2d.fillRect(0, 0, width, height);
	        	for(int i = 0; i < 780; i=i+200) {
	            	for(int j = 0; j < 1688; j=j+251) {
	            		g2d.setColor(new Color(R, B, G));
	                    g2d.fillOval(i, j, 179, 179);
	            	} 
	            }
	        }
	        
	        else if(randomBackgroundNumber>=rectanglesNumber+circlesNumber 
	        		&& randomBackgroundNumber<(rectanglesNumber+circlesNumber+squaresNumber)) {
	        	metadataBackground = "Squares";
	        	g2d.setColor(new Color(R, B, G));
	            g2d.fillRect(0, 0, width, height);
	        	g2d.setColor(new Color(B, R, G));
	        	g2d.fillRect(0, 0, 390, 844);
	        	g2d.setColor(new Color(B, R, G));
	        	g2d.fillRect(390, 844, 390, 844);
	        }
	        
	        else if(randomBackgroundNumber>=rectanglesNumber+circlesNumber+squaresNumber 
	        		&& randomBackgroundNumber<(rectanglesNumber+circlesNumber+squaresNumber+patternNumber)) {
	        	metadataBackground = "Pattern";
	        	g2d.setColor(new Color (B, R, G));
	            g2d.fillRect(0, 0, width, height);
	        	g2d.setColor(new Color(R, G, B));
	        	g2d.fillRect(0, 0, 390, 844);
	        	g2d.fillRect(390, 844, 390, 844);
	        	for(int i = 0; i < 780; i=i+200) {
	            	for(int j = 0; j < 1688; j=j+251) {
	            		g2d.setColor(new Color(B2, R2, G2));
	                    g2d.fillOval(i, j, 179, 179);
	            	}
	            }
	        	for(int i = 0; i < 780; i=i+82) {
	            	for(int j = 0; j < 1688; j=j+274) {
	            		g2d.setColor(new Color(R2, G2, B2));
	                    g2d.fillRect(i, j, 42, 42);
	            	} 
	            }
	        }
	        else if(randomBackgroundNumber>=rectanglesNumber+circlesNumber+squaresNumber+patternNumber 
	        		&& randomBackgroundNumber<(rectanglesNumber+circlesNumber+squaresNumber+patternNumber+staticNumber)) {
	        	metadataBackground = "Static";
	        	g2d.setColor(new Color(R, G, B));
	            g2d.fillRect(0, 0, width, height);
        }}
		return bufferedImage;	
	}

    /**
     * Generates a BufferedImage based on the input from the user
     * Values: human, alien, zombie.
     * the default value is human
     * 
     * return creature/body BufferedImage
     */
	@Override
	public BufferedImage body() {
		
		int humanNumber = FrameGenerator.human;
		int alienNumber = FrameGenerator.alien;
		int zombieNumber = FrameGenerator.zombie;
		
		
		int totalNumber = humanNumber + alienNumber + zombieNumber;
		
		//random.nextInt("can not be 0")
		if(totalNumber>0) {
			int randomBodyNumber = random.nextInt(totalNumber);
	
	        //omriss
	        g2d.setColor(Color.black);
	        g2d.fillRect(337, 1340, 106, 106);
	        //hand omriss
	        g2d.fillRect(192, 1560-20, 91, 41);
	        g2d.fillRect(502, 1560-20, 91, 41);
	        
	      
        
	        if(randomBodyNumber>=0 && randomBodyNumber<humanNumber) {
	        	List<Color> skinColorList = RainbowColors.getRandomSkin(RainbowColors.allSkin);
	    		Color skinTone = RainbowColors.getSkinTone(skinColorList);
	    		Color skinContrast = RainbowColors.getSkinContrast(skinColorList);
	        	
	        	this.metadataBody = "Human";
	        	//hals
	            g2d.setColor(skinTone);
	            g2d.fillRect(340, 1340, 100, 100);
	            
	            g2d.setColor(Color.black);
	            g2d.fillOval(63, 342, 654, 1004);
	            //hode
	        	g2d.setColor(skinTone);
	            g2d.fillOval(65, 344, 650, 1000);
	            //hand
	            g2d.fillRect(195, 1563-20, 85, 35);
	            g2d.fillRect(505, 1563-20, 85, 35);
	          //�re omriss
	            g2d.setColor(Color.black);
	            g2d.fillOval(35-2, 697, 64, 236);
		        g2d.fillOval(685-1, 697, 64, 236);
	            //nase
	            g2d.setColor(skinContrast);
	            g2d.fillArc(340, 950, 100, 100, 135, 270);
	            g2d.fillArc(300, 700, 180, 300, 253, 35);
	          //�re
	            g2d.fillOval(35, 700, 60, 230);
	            g2d.fillOval(685, 700, 60, 230);
	          //�yre kryss
	            g2d.setColor(skinTone);
	            //h�gre
	            g2d.drawLine(715, 705, 715, 925);
	            g2d.drawLine(690, 810, 740, 810);
	            //venstre
	            g2d.drawLine(65, 705, 65, 925);
	            g2d.drawLine(40, 810, 90, 810);
	        }
	        
	        else if(randomBodyNumber>=humanNumber && randomBodyNumber<(humanNumber+alienNumber)) {
	        	alien = true;
	        	this.metadataBody = "Alien";
	        	//alien=true;
	        	//hals
	            g2d.setColor(new Color(108, 196, 23));
	            g2d.fillRect(340, 1340, 100, 100);
	            
	            g2d.setColor(Color.black);
	            g2d.fillOval(63, 342, 654, 1004);
	            //hode
	        	g2d.setColor(new Color(108, 196, 23));
	            g2d.fillOval(65, 344, 650, 1000);
	            g2d.fillRect(195, 1563-20, 85, 35);
	            g2d.fillRect(505, 1563-20, 85, 35);
	          //�re omriss
	            g2d.setColor(Color.black);
	            g2d.fillOval(35-2, 697, 64, 236);
		        g2d.fillOval(685-1, 697, 64, 236);
	          //nase
	            g2d.setColor(new Color(108+20, 196+20, 23+20));
	            g2d.fillArc(340, 950, 100, 100, 135, 270);
	            g2d.fillArc(300, 700, 180, 300, 253, 35);
	            //�re
	            g2d.fillOval(35, 700, 60, 230);
	            g2d.fillOval(685, 700, 60, 230);
	            //�yre kryss
	            g2d.setColor(new Color(153+20,50+20,204+20));
	            //h�gre
	            g2d.drawLine(715, 705, 715, 925);
	            g2d.drawLine(690, 810, 740, 810);
	            //venstre
	            g2d.drawLine(65, 705, 65, 925);
	            g2d.drawLine(40, 810, 90, 810);
	
	        }
	        if(randomBodyNumber>=humanNumber+alienNumber && randomBodyNumber<(humanNumber+alienNumber+zombieNumber)) {
	        	this.metadataBody = "Zombie";
	        	//hals
	            g2d.setColor(new Color(83, 154, 119));
	            g2d.fillRect(340, 1340, 100, 100);
	            
	            g2d.setColor(Color.black);
	            g2d.fillOval(63, 342, 654, 1004);
	            //hode
	        	g2d.setColor(new Color(83, 154, 119));
	            g2d.fillOval(65, 344, 650, 1000);
	            g2d.fillRect(195, 1563-20, 85, 35);
	            g2d.fillRect(505, 1563-20, 85, 35);
	          //�re omriss
	            g2d.setColor(Color.black);
	            g2d.fillOval(35-2, 697, 64, 236);
		        g2d.fillOval(685-1, 697, 64, 236);
	            //nase
	            g2d.setColor(new Color(83-20, 154-20, 119-20));
	            g2d.fillArc(340, 950, 100, 100, 135, 270);
	            //g2d.fillArc(300, 700, 180, 300, 253, 35);
	          //�re
	            g2d.fillOval(35, 700, 60, 230);
	            g2d.fillOval(685, 700, 60, 230);
	          //�yre kryss
	            g2d.setColor(new Color(209, 0, 28));
	            //h�gre
	            g2d.drawLine(715, 705, 715, 925);
	            g2d.drawLine(690, 810, 740, 810);
	            //venstre
	            g2d.drawLine(65, 705, 65, 925);
	            g2d.drawLine(40, 810, 90, 810);
	            //dotts
	            g2d.setColor(new Color(96, 141, 139));
	            g2d.fillOval(300, 400, 50, 60);
	            g2d.fillOval(320, 1050, 50, 60);
	            g2d.fillOval(620, 750, 50, 60);
	            g2d.fillOval(220, 750, 50, 30);
	            g2d.fillOval(320, 550, 50, 50);
	            g2d.fillOval(320, 1200, 50, 50);
	            g2d.fillOval(520, 950, 50, 50);
	            g2d.fillOval(460, 1250, 50, 50);
	            g2d.fillOval(250, 1150, 50, 50);
	            g2d.fillOval(200, 850, 70, 80);
	            g2d.fillOval(400, 650, 70, 80);
	            g2d.fillOval(550, 500, 70, 80);
	            g2d.fillOval(590, 1100, 30, 40);
	            
	            g2d.setColor(new Color(209, 0, 28));
	            g2d.drawLine(130, 950-8, 300, 1100-8);
	            g2d.drawLine(130, 950-4, 300, 1100-4);
	            g2d.drawLine(130, 950, 300, 1100);
	            g2d.drawLine(130, 952, 300, 1102);
	            g2d.drawLine(130, 956, 300, 1106);
	            g2d.drawLine(130, 959, 300, 1109);
	            
	            g2d.drawLine(180, 959, 280, 1109);
	            g2d.drawLine(180, 959+4, 280, 1109+4);
	            g2d.drawLine(180, 959+8, 280, 1109+8);
	            g2d.drawLine(180, 959+12, 280, 1109+12);
	            
	            g2d.setColor(new Color(218, 51, 73));
	            g2d.fillOval(230, 1050, 6, 20);
	            g2d.fillOval(240, 1080, 6, 20);
	            g2d.fillOval(200, 1020, 6, 20);
	            g2d.fillOval(215, 1060, 6, 20);
	            g2d.fillOval(225, 1100, 6, 20);    
        }}
		return bufferedImage;
	}

	
	/**
	 * Generates a BufferedImage based on the input from the user
     * Values: upAndDown, narrow, upAndNormal, bigNormal, small, lizzard, bigIris, 
	 * lookingOutwards, lookingInwards, lookingDownwards, lookingUpwards
     * the default value is bigNormal
     * 
     * return eyes BufferedImage
	 */
	@Override
	public BufferedImage eyes() {
 
		
		int upAndDown = FrameGenerator.upAndDown;
		int narrow = FrameGenerator.narrow;
		int upAndNormal = FrameGenerator.upAndNormal;
		int bigNormal = FrameGenerator.bigNormal;
		int small = FrameGenerator.small;
		int lizzard = FrameGenerator.lizzard;
		int bigIris = FrameGenerator.bigIris;
		int lookingOutwards = FrameGenerator.lookingOutwards;
		int lookingInwards = FrameGenerator.lookingInwards;
		int lookingDownwards = FrameGenerator.lookingDownwards;
		int lookingUpwards = FrameGenerator.lookingUpwards;
		
		int total = upAndDown+narrow+upAndNormal+bigNormal+small+lizzard+bigIris+
				    lookingOutwards+lookingInwards+lookingDownwards+lookingUpwards;
		
		
        Color randomEyeColor = RainbowColors.getRandomColor(RainbowColors.allColors);
 
       //random.nextInt("can not be 0")
        if(total>0) {
        	int randomEyeNumber = random.nextInt(total);
	 
			if(randomEyeNumber>=0 && randomEyeNumber<upAndDown) {
	        	this.metadataEye = "Up and down";
	        	g2d.setColor(black);
	            g2d.fillOval(217, 698, 125, 175);
	            g2d.fillOval(438, 719, 123, 174);
	            g2d.setColor(white);
	            g2d.fillOval(220, 700, 120, 190);
	            g2d.fillOval(440, 700, 120, 190);
	            g2d.setColor(randomEyeColor);
	            g2d.fillOval(246, 704, 60, 70);
	            g2d.fillOval(464, 815, 60, 70);
	            g2d.setColor(black);
	            g2d.fillOval(260, 715, 32, 40);
	            g2d.fillOval(477, 835, 32, 40);
	            g2d.setColor(red);
	            g2d.drawString("/////////////", 260, 890);
	            g2d.drawString("//////////////////////", 248, 880);
	            g2d.drawString("/////                 /////", 242, 870);
	            g2d.drawString("/////                 /////", 462, 730);
	            g2d.drawString("/////////////", 480, 710);
	            g2d.drawString("//////////////////////", 468, 720);
	        }
	        else if(randomEyeNumber>=upAndDown && randomEyeNumber<upAndDown+narrow) {
	        	metadataEye = "Narrow";
	        	g2d.setColor(black);
	            g2d.fillOval(187, 766, 186, 37);
	            g2d.fillOval(407, 766, 186, 37);
	        	g2d.setColor(white);
	            g2d.fillOval(190, 767, 180, 35);
	            g2d.fillOval(410, 767, 180, 35);
	            g2d.setColor(randomEyeColor);
	            g2d.fillOval(310, 767, 35, 35);
	            g2d.fillOval(435, 767, 35, 35);
	            g2d.setColor(black);
	            g2d.fillOval(327, 777, 15, 15);
	            g2d.fillOval(438, 777, 15, 15);
	            g2d.setColor(red);
	            g2d.fillRect(192, 779, 15, 11);
	            g2d.fillRect(573, 779, 15, 11);
	        }
	        else if(randomEyeNumber>=upAndDown+narrow && randomEyeNumber<(upAndDown+narrow+upAndNormal)) {
	        	metadataEye = "Up and normal";
	        	g2d.setColor(black);
	            g2d.fillOval(217, 698, 125, 175);
	            g2d.fillOval(437, 708, 125, 174);
	            g2d.setColor(white);
	            g2d.fillOval(220, 700, 120, 190);
	            g2d.fillOval(440, 700, 120, 190);
	            g2d.setColor(randomEyeColor);
	            g2d.fillOval(246, 704, 60, 70);
	            g2d.fillOval(466, 747, 60, 70);
	            g2d.setColor(black);
	            g2d.fillOval(260, 715, 32, 40);
	            g2d.fillOval(480, 760, 32, 40);
	            g2d.setColor(red);
	            g2d.drawString("/////////////", 260, 890);
	            g2d.drawString("//////////////////////", 248, 880);
	            g2d.drawString("/////                 /////", 242, 870);
	
	        }
	        else if(randomEyeNumber>=upAndDown+narrow+upAndNormal && randomEyeNumber<upAndDown+narrow+upAndNormal+bigNormal) {
	        	metadataEye = "Big normal";
	        	g2d.setColor(black);
	            g2d.fillOval(217, 698, 125, 175);
	            g2d.fillOval(438, 700, 124, 165);
	            g2d.setColor(white);
	            g2d.fillOval(220, 700, 120, 190);
	            g2d.fillOval(440, 700, 120, 190);
	            g2d.setColor(black);
	            g2d.fillOval(234, 740, 94, 116);
	            g2d.fillOval(454, 740, 94, 116);
	            g2d.setColor(randomEyeColor);
	            g2d.fillOval(236, 740, 90, 110);
	            g2d.fillOval(456, 740, 90, 110);
	            g2d.setColor(black);
	            g2d.fillOval(260, 770, 32, 40);
	            g2d.fillOval(480, 770, 32, 40);
	            g2d.setColor(red);
	            g2d.drawString("/////////////", 260, 890);
	            g2d.drawString("//////////////////////", 248, 880);
	            g2d.drawString("/////                 /////", 242, 870);
	            g2d.drawString("////////////", 485, 711);
	            g2d.drawString("/////////////////////", 470, 721);
	            g2d.drawString("////////////", 485, 731);
	        }
	        else if(randomEyeNumber>=upAndDown+narrow+upAndNormal+bigNormal 
	        		&& randomEyeNumber<upAndDown+narrow+upAndNormal+bigNormal+small) {
	        	metadataEye = "Small";
	        	g2d.setColor(black);
	            g2d.fillOval(205, 755, 143, 53);
	            g2d.fillOval(425, 755, 143, 53);
	            g2d.setColor(white);
	            g2d.fillOval(205, 755, 145, 50);
	            g2d.fillOval(425, 755, 145, 50);
	            g2d.setColor(randomEyeColor);
	            g2d.fillOval(253, 755, 56, 50);
	            g2d.fillOval(473, 755, 56, 50);
	            g2d.setColor(black);
	            g2d.fillOval(263, 763, 34, 34);
	            g2d.fillOval(483, 763, 34, 34);
	        }
	        else if(randomEyeNumber>=upAndDown+narrow+upAndNormal+bigNormal+small 
	        		&& randomEyeNumber<upAndDown+narrow+upAndNormal+bigNormal+small+lizzard) {
	        	metadataEye = "Lizzard";
	        	g2d.setColor(black);
	            g2d.fillOval(205, 770, 143, 53);
	            g2d.fillOval(425, 770, 143, 53);
	            g2d.fillOval(205, 736, 143, 53);
	            g2d.fillOval(425, 736, 143, 53);
	            g2d.setColor(white);
	            g2d.fillOval(205, 745, 145, 70);
	            g2d.fillOval(425, 745, 145, 70);
	            g2d.setColor(black);
	            g2d.fillOval(259, 745, 43, 73);
	            g2d.fillOval(479, 745, 43, 73);
	            g2d.setColor(randomEyeColor);
	            g2d.fillOval(259, 745, 40, 70);
	            g2d.fillOval(479, 745, 40, 70);
	            g2d.setColor(black);
	            g2d.fillOval(271, 763, 14, 34);
	            g2d.fillOval(491, 763, 14, 34);
	        }
	        else if(randomEyeNumber>=upAndDown+narrow+upAndNormal+bigNormal+small+lizzard 
	        		&& randomEyeNumber<upAndDown+narrow+upAndNormal+bigNormal+small+lizzard+bigIris) {
	        	metadataEye = "Big iris";
	        	g2d.setColor(black);
	            g2d.fillOval(180, 740, 197, 100);
	            g2d.fillOval(400, 740, 197, 100);
	            g2d.setColor(white);
	            g2d.fillOval(190, 740, 180, 100);
	            g2d.fillOval(410, 740, 180, 100);
	            g2d.setColor(black);
	            g2d.fillOval(234, 740, 94, 116);
	            g2d.fillOval(454, 740, 94, 116);
	            g2d.setColor(randomEyeColor);
	            g2d.fillOval(236, 740, 90, 110);
	            g2d.fillOval(456, 740, 90, 110);
	            g2d.setColor(black);
	            g2d.fillOval(260, 770, 32, 40);
	            g2d.fillOval(480, 770, 32, 40);
	            g2d.setColor(randomEyeColor);
	            g2d.fillOval(267, 780, 20, 20);
	            g2d.fillOval(487, 780, 20, 20);
	            g2d.setColor(black);
	            g2d.fillOval(274, 787, 6, 8);
	            g2d.fillOval(494, 787, 6, 8);
	        }
	      //ser utover
	        else if(randomEyeNumber>=upAndDown+narrow+upAndNormal+bigNormal+small+lizzard+bigIris 
	        		&& randomEyeNumber<upAndDown+narrow+upAndNormal+bigNormal+small+lizzard+bigIris+lookingOutwards) {
	        	metadataEye = "Looking outwards";
	        	g2d.setColor(black);
	            g2d.fillOval(217, 700, 123, 193);
	            g2d.fillOval(440, 700, 123, 193);
	            g2d.setColor(white);
	            g2d.fillOval(220, 700, 120, 190);
	            g2d.fillOval(440, 700, 120, 190);
	            g2d.setColor(randomEyeColor);
	            g2d.fillOval(225, 755, 60, 70);
	            g2d.fillOval(495, 755, 60, 70);
	            g2d.setColor(black);
	            g2d.fillOval(235, 770, 32, 40);
	            g2d.fillOval(513, 770, 32, 40);
	        }
	      //ser innover
	        else if(randomEyeNumber>=upAndDown+narrow+upAndNormal+bigNormal+small+lizzard+bigIris+lookingOutwards 
	        		&& randomEyeNumber<upAndDown+narrow+upAndNormal+bigNormal+
	        		small+lizzard+bigIris+lookingOutwards+lookingInwards) {
	        	metadataEye = "Looking innwards";
	        	g2d.setColor(black);
	            g2d.fillOval(220, 700, 123, 193);
	            g2d.fillOval(437, 700, 123, 193);
	            g2d.setColor(white);
	            g2d.fillOval(220, 700, 120, 190);
	            g2d.fillOval(440, 700, 120, 190);
	            g2d.setColor(randomEyeColor);
	            g2d.fillOval(275, 755, 60, 70);
	            g2d.fillOval(445, 755, 60, 70);
	            g2d.setColor(black);
	            g2d.fillOval(295, 770, 32, 40);
	            g2d.fillOval(455, 770, 32, 40);
	        }
	        //ser nedover
	        else if(randomEyeNumber>=upAndDown+narrow+upAndNormal+bigNormal+small+lizzard+bigIris+lookingOutwards+lookingInwards 
	        		&& randomEyeNumber<upAndDown+narrow+upAndNormal+bigNormal+
	        		small+lizzard+bigIris+lookingOutwards+lookingInwards+lookingDownwards) {
	        	metadataEye = "Looking downwards";
	        	g2d.setColor(black);
	        	g2d.fillOval(220, 700, 120, 194);
	            g2d.fillOval(440, 700, 120, 194);
	            g2d.setColor(white);
	            g2d.fillOval(220, 700, 120, 190);
	            g2d.fillOval(440, 700, 120, 190);
	            g2d.setColor(randomEyeColor);
	            g2d.fillOval(265, 805, 60, 70);
	            g2d.fillOval(455, 805, 60, 70);
	            g2d.setColor(black);
	            g2d.fillOval(281, 825, 32, 40);
	            g2d.fillOval(467, 825, 32, 40);
	        }
	        else if(randomEyeNumber>=upAndDown+narrow+upAndNormal+bigNormal+small+lizzard+bigIris+lookingOutwards+lookingInwards+lookingDownwards 
	        		&& randomEyeNumber<upAndDown+narrow+upAndNormal+bigNormal+
	        		small+lizzard+bigIris+lookingOutwards+lookingInwards+lookingDownwards+lookingUpwards) {
	        	metadataEye = "Looking upwnwards";
	        	g2d.setColor(black);
	        	g2d.fillOval(220, 697, 120, 194);
	            g2d.fillOval(440, 697, 120, 194);
	            g2d.setColor(white);
	            g2d.fillOval(220, 700, 120, 190);
	            g2d.fillOval(440, 700, 120, 190);
	            g2d.setColor(randomEyeColor);
	            g2d.fillOval(246, 704, 60, 70);
	            g2d.fillOval(466, 704, 60, 70);
	            g2d.setColor(black);
	            g2d.fillOval(260, 715, 32, 40);
	            g2d.fillOval(480, 715, 32, 40);
	            g2d.setColor(red);
	            g2d.drawString("/////////////", 260, 890);
	            g2d.drawString("//////////////////////", 248, 880);
	            g2d.drawString("/////                 /////", 242, 870);
	            g2d.drawString("/////////////", 480, 890);
	            g2d.drawString("//////////////////////", 468, 880);
	            g2d.drawString("/////                 /////", 462, 870);
	        }
      }
		return bufferedImage;
	}

	
	/**
	 * Generates a BufferedImage based on the input from the user
     * Values: upAndDown, narrow, upAndNormal, bigNormal, small, lizzard, bigIris, 
	 * lookingOutwards, lookingInwards, lookingDownwards, lookingUpwards
     * the default value is bigNormal
     * 
     * return eyes BufferedImage
	 */
	@Override
	public  BufferedImage mouth() {

		int orc = FrameGenerator.orc;
		int snake = FrameGenerator.snake;
		int retarded = FrameGenerator.retarded;
		int normal = FrameGenerator.normal;
		int british = FrameGenerator.british;
		int underbite = FrameGenerator.underbite;
		int allTeeths = FrameGenerator.allTeeths;
		
		int total = orc+snake+retarded+normal+british+underbite+allTeeths;
        

        List<Color> toothColor = Arrays.asList(yellow, brown, brown, white, white);
        Color randomToothColor = toothColor.get(random.nextInt(toothColor.size()));
        
      //random.nextInt("can not be 0")
      	if(total>0) {
	        int randomMouthNumber = random.nextInt(total);
			//munn med 4 tenner
	        if(randomMouthNumber>=0 && randomMouthNumber<orc) {
	        	metadataMouth = "Orc";
	        	g2d.setColor(red);//leppe
	            g2d.fillOval(270, 1100, 240, 100);
	            g2d.fillOval(280, 1100, 220, 130);
	            g2d.fillOval(290, 1070, 120, 130);
	            g2d.fillOval(370, 1070, 120, 130);
	        	g2d.setColor(Color.black);
	            g2d.fillOval(280, 1100, 220, 100);
	            g2d.setColor(randomToothColor);//tann
	            g2d.fillRect(355, 1160, 30, 40);
	            g2d.fillRect(395, 1160, 30, 40);
	            g2d.fillOval(355, 1145, 30, 30);
	            g2d.fillOval(395, 1145, 30, 30);
	            g2d.fillRect(310, 1115, 30, 40);
	            g2d.fillRect(440, 1115, 30, 40);
	            g2d.fillOval(310, 1125, 30, 40);
	            g2d.fillOval(440, 1125, 30, 40);
	        }
	      //hoggtann kvit
	        else if(randomMouthNumber>=orc && randomMouthNumber<(orc+snake)) {
	        	metadataMouth = "Snake";
	        	g2d.setColor(red);//leppe
	            g2d.fillOval(270, 1085, 240, 120);
	            g2d.fillOval(280, 1100, 220, 130);
	            g2d.fillOval(290, 1070, 120, 130);
	            g2d.fillOval(370, 1070, 120, 130);
	        	g2d.setColor(Color.black);
	            g2d.fillOval(280, 1100, 220, 100);
	            g2d.setColor(randomToothColor);//tann
	            g2d.fillRect(350, 1105, 30, 140);
	            g2d.fillRect(400, 1105, 30, 140);
	            g2d.fillOval(350, 1225, 30, 30);
	            g2d.fillOval(400, 1232, 30, 30);
	        }
	      //retard kvit 
	        else if(randomMouthNumber>=orc+snake && randomMouthNumber<(orc+snake+retarded)) {
	        	metadataMouth = "Retarded";
	        	g2d.setColor(red);//leppe
	            g2d.fillOval(270, 1085, 240, 110);
	        	g2d.setColor(Color.black);
	            g2d.fillOval(290, 1100, 200, 80);
	            g2d.setColor(randomToothColor);//tann
	            g2d.fillRect(335, 1108, 30, 50);
	            g2d.fillRect(375, 1102, 30, 50);
	            g2d.fillRect(415, 1108, 30, 50);
	            g2d.fillRect(455, 1124, 30, 25);
	            g2d.fillRect(295, 1124, 30, 24); 
	        }
	      //vanlig 6 tann kvit
	        else if(randomMouthNumber>=orc+snake+retarded && randomMouthNumber<(orc+snake+retarded+normal)) {
	        	metadataMouth = "Normal";
	        	g2d.setColor(red);//leppe
	            g2d.fillOval(270, 1085, 240, 110);
	        	g2d.setColor(Color.black);
	            g2d.fillOval(290, 1100, 200, 80);
	            g2d.setColor(randomToothColor);//tann
	            g2d.fillRect(335, 1108, 30, 25);
	            g2d.fillRect(375, 1102, 30, 25);
	            g2d.fillRect(415, 1108, 30, 25);
	            g2d.fillOval(335, 1112, 30, 25);
	            g2d.fillOval(375, 1108, 30, 25);
	            g2d.fillOval(415, 1112, 30, 25);
	            g2d.fillRect(335, 1148, 30, 25);
	            g2d.fillRect(375, 1152, 30, 25);
	            g2d.fillRect(415, 1148, 30, 25);
	            g2d.fillOval(335, 1144, 30, 25);
	            g2d.fillOval(375, 1146, 30, 25);
	            g2d.fillOval(415, 1144, 30, 25);
	        }
	      //britisk kvit
	        else if(randomMouthNumber>=orc+snake+retarded+normal && randomMouthNumber<(orc+snake+retarded+normal+british)) {
	        	metadataMouth = "British";
	        	g2d.setColor(Color.black);
	            g2d.fillOval(320, 1040, 140, 240);
	        	g2d.setColor(red);//leppe
	            g2d.fillOval(320, 1045, 140, 230);
	        	g2d.setColor(Color.black);
	            g2d.fillOval(340, 1060, 100, 200);
	            g2d.setColor(randomToothColor);//tann
	            g2d.fillRect(357, 1088, 12, 146);
	            g2d.fillRect(375, 1068, 12, 186);
	            g2d.fillRect(393, 1068, 12, 186);
	            g2d.fillRect(411, 1088, 12, 146);
	            g2d.setColor(Color.black);
	            g2d.fillRect(355, 1150, 14, 25);
	            g2d.fillRect(375, 1170, 14, 40);
	            g2d.fillRect(392, 1140, 14, 45);
	            g2d.fillRect(410, 1150, 14, 15);
	        }
	      //underbitt kvit
	        else if(randomMouthNumber>=orc+snake+retarded+normal+british &&
	        		randomMouthNumber<(orc+snake+retarded+normal+british+underbite)) {
	        	metadataMouth = "Underbite";
	        	g2d.setColor(red);//leppe
	            g2d.fillOval(270, 1085, 240, 120);
	        	g2d.setColor(Color.black);
	            g2d.fillOval(290, 1100, 200, 90);
	            g2d.setColor(randomToothColor);//tann
	            g2d.fillRect(335, 1138, 30, 42);
	            g2d.fillRect(375, 1150, 30, 40);
	            g2d.fillRect(415, 1138, 30, 42);
	            g2d.fillRect(455, 1128, 30, 25);
	            g2d.fillRect(295, 1128, 30, 24); 
	        }
	      //full tann kvit
	        else if(randomMouthNumber>=orc+snake+retarded+normal+british+underbite &&
	        		randomMouthNumber<(orc+snake+retarded+normal+british+underbite+allTeeths)) {
	        	metadataMouth = "All teeths";
	        	g2d.setColor(red);//leppe
	            g2d.fillOval(270, 1085, 240, 120);
	        	g2d.setColor(Color.black);
	            g2d.fillOval(290, 1100, 200, 90);
	            g2d.setColor(randomToothColor);//tann
	            g2d.fillRect(335, 1150, 30, 30);
	            g2d.fillRect(375, 1155, 30, 30);
	            g2d.fillRect(415, 1150, 30, 30);
	            g2d.fillRect(455, 1128, 30, 25);
	            g2d.fillRect(295, 1128, 30, 24);
	            g2d.fillRect(335, 1110, 30, 30);
	            g2d.fillRect(375, 1105, 30, 30);
	            g2d.fillRect(415, 1110, 30, 30);
        }}
		return bufferedImage;
	}

	@Override
	public BufferedImage hair() {
		//farger brukt i h�r
        Color brownHair = new Color(103, 47, 21);
        Color darkBlonde = new Color(156, 120, 63);
        Color darkBrownHair = new Color(78, 22, 25);
        Color blonde = new Color(239, 199, 86);
        Color greenJoker = new Color(46, 125, 50);
        Color purpleAlien = new Color(133, 77, 220);
        

        List<Color> hairColor = Arrays.asList(brownHair, darkBlonde, darkBrownHair, blonde, greenJoker, purpleAlien);
        Color randomHairColor = hairColor.get(random.nextInt(hairColor.size()));
        
        int ziggZagg = FrameGenerator.ziggZagg;
        int curly = FrameGenerator.curly;
        int pherb = FrameGenerator.pherb;
        int jimmy = FrameGenerator.jimmy;
        int total = ziggZagg+curly+pherb+jimmy;
        
      //random.nextInt("can not be 0")
      	if(total>0) {
	        int randomHairNumber = random.nextInt(total);
	        
	        if(randomHairNumber>=0 && randomHairNumber<ziggZagg) {
	        	metadataHair = "Zigg zagg";
	            for(int i = 80; i < 250; i=i+20) {
	            	g2d.setColor(randomHairColor);
	                g2d.fillArc(100+i, 250+i, 350, 80, 10, 40);
	                g2d.fillArc(200-i, 250+i, 450, 80, 10, 110); 
	            }
	        }
	        else if(randomHairNumber>=ziggZagg && randomHairNumber<ziggZagg+curly) {
	        	metadataHair = "Curly";
	            for(int i = 80; i < 250; i=i+30) {
	            	g2d.setColor(randomHairColor);
	            	g2d.fillArc(10+(i/2), 200+i, 660, 110, 210, 110);
	                g2d.fillArc(200-i, 250+2*i, 350, 100, 80, 130);
	                g2d.fillArc(200+i, 200+2*i, 350, 110, 250, 160);
	            }
	        }
	        else if(randomHairNumber>=ziggZagg+curly && randomHairNumber<ziggZagg+curly+pherb) {
	        	metadataHair = "Pherb";
	            for(int i = 20; i < 100; i=i+10) {
	            	g2d.setColor(randomHairColor);
	            	g2d.fillArc(150, 320, 850, 90, 100, 100);
	                g2d.fillArc(200-i, 250+2*i, 350, 100, 80, 130);
	                g2d.fillArc(200+i, 200+2*i, 350, 110, 250, 160);
	            }
	        }
	        else if(randomHairNumber>=ziggZagg+curly+pherb && randomHairNumber<ziggZagg+curly+pherb+jimmy) {
	        	metadataHair = "Jimmy";
	            for(int i = 20; i < 120; i=i+25) {
	            	g2d.setColor(randomHairColor);
	            	g2d.fillArc(150+i, 460-i*2, 350+i, 160, 100, 100+i*10);
	                g2d.fillArc(200-i, 250+2*i, 350, 100, 80, 130);
	                g2d.fillArc(70-i, 450+2*i, 350, 100, 80, 130);
	                g2d.fillArc(200+i, 200+2*i, 350, 110, 250, 160);
	                g2d.fillArc(330+i, 380+2*i, 350, 110, 250, 160);
            }
        }}
		return bufferedImage;
	}
	
	public BufferedImage alienAntenna() {
		if(alien) {
			//antenna
            g2d.setColor(Color.black);
            g2d.fillOval(150, 120, 80, 80);
            g2d.fillOval(550, 120, 80, 80);
            for(int i=0; i<160; i=i+8) {
                g2d.fillRect(328-i, 447-(i*2), 27, 27);
                g2d.fillRect(430+i, 447-(i*2), 27, 27);
            }
            g2d.setColor(new Color(108+30, 196+30, 23+30));
            g2d.fillOval(155, 125, 70, 70);
            g2d.fillOval(555, 125, 70, 70);
            for(int i=0; i<160; i=i+8) {
                g2d.fillRect(330-i, 450-(i*2), 25, 25);
                g2d.fillRect(430+i, 450-(i*2), 25, 25);
            }
            g2d.setColor(new Color(153,50,204));
            g2d.fillOval(170, 140, 40, 40);
            g2d.fillOval(570, 140, 40, 40);
		}
		
		return bufferedImage;
	}
	
	
	
	public BufferedImage nose() {
		g2d.setColor(Color.black);
        g2d.fillOval(350, 1000, 30, 30);
        g2d.fillOval(400, 1000, 30, 30);
		return bufferedImage;
	}

	//m� fiksast
	@Override
	public BufferedImage eyebrows() {
		
		//normalEyebrow, angry, sad, unhappy, superangry, casual,
		 //  casualv2, normalv2, shocked, dissapointed.
		
		int normalEyebrow = FrameGenerator.normalEyebrow;
		int angry = FrameGenerator.angry;
		int sad = FrameGenerator.sad;
		int unhappy = FrameGenerator.unhappy;
		int superangry = FrameGenerator.superAngry;
		int casual = FrameGenerator.casual;
		int casualv2 = FrameGenerator.casualv2;
		int normalv2 = FrameGenerator.normalv2;
		int shocked = FrameGenerator.shocked;
		int dissappointed = FrameGenerator.dissappointed;
		
		int total = normalEyebrow+angry+sad+unhappy+superangry+casual+casualv2+normalv2+shocked+dissappointed;
		
		//random.nextInt("can not be 0")
      	if(total>0) {
			int randomEyebrowNumber = random.nextInt(total);
			
	            if(randomEyebrowNumber>=0 && randomEyebrowNumber<normalEyebrow) {
	            	metadataEyebrows = "Normal";
	            	for(int i=0; i<160; i=i+1) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 670-(i/30), 20, 25);
	                    g2d.fillRect(420+i, 670-(i/30), 20, 25);
	                }  
	            }
	            else if(randomEyebrowNumber>=normalEyebrow && randomEyebrowNumber<normalEyebrow+angry) {
	            	metadataEyebrows = "Angry";
	                for(int i=0; i<160; i=i+10) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 690-(i/2), 20, 25);
	                    g2d.fillRect(420+i, 690-(i/2), 20, 25);
	                }    
	            }
	            else if(randomEyebrowNumber>=normalEyebrow+angry && randomEyebrowNumber<normalEyebrow+angry+sad) {
	            	metadataEyebrows = "Sad";
	                for(int i=0; i<160; i=i+10) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 615+(i/2), 20, 25);
	                    g2d.fillRect(420+i, 615+(i/2), 20, 25);
	                }    
	            }
	            else if(randomEyebrowNumber>=normalEyebrow+angry+sad && randomEyebrowNumber<normalEyebrow+angry+sad+unhappy) {
	            	metadataEyebrows = "Unhappy";
	                for(int i=0; i<160; i=i+10) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 640+(i/5), 20, 25);
	                    g2d.fillRect(420+i, 640+(i/5), 20, 25);
	                }    
	            }
	            else if(randomEyebrowNumber>=normalEyebrow+angry+sad+unhappy 
	            		&& randomEyebrowNumber<normalEyebrow+angry+sad+unhappy+superangry) {
	            	metadataEyebrows = "Superangry";
	                for(int i=0; i<140; i=i+10) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 710-(i), 25, 25);
	                    g2d.fillRect(420+i, 710-(i), 25, 25);
	                }    
	            }
	            else if(randomEyebrowNumber>=normalEyebrow+angry+sad+unhappy+superangry 
	            		&& randomEyebrowNumber<normalEyebrow+angry+sad+unhappy+superangry+casual) {
	            	metadataEyebrows = "Casual";
	            	for(int i=0; i<80; i=i+10) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 670-(i/20), 20, 25);
	                    g2d.fillRect(420+i, 670-(i/20), 20, 25);
	                    g2d.fillRect(260-i, 670+(i/3), 20, 25);
	                    g2d.fillRect(500+i, 670+(i/3), 20, 25);
	                } 
	            }
	            else if(randomEyebrowNumber>=normalEyebrow+angry+sad+unhappy+superangry+casual 
	            		&& randomEyebrowNumber<normalEyebrow+angry+sad+unhappy+superangry+casual+casualv2) {
	            	metadataEyebrows = "Casualv2";
	            	for(int i=0; i<35; i=i+10) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 660+(i/3), 20, 25);
	                    g2d.fillRect(420+i, 660+(i/3), 20, 25);
	                    g2d.fillRect(290-i, 670-(i/3), 20, 25);
	                    g2d.fillRect(470+i, 670-(i/3), 20, 25);
	                    g2d.fillRect(240-i, 660+(i/3), 20, 25);
	                    g2d.fillRect(520+i, 660+(i/3), 20, 25);
	                    g2d.fillRect(190-i, 670-(i/3), 20, 25);
	                    g2d.fillRect(570+i, 670-(i/3), 20, 25);
	                }    
	            }
	            else if(randomEyebrowNumber>=normalEyebrow+angry+sad+unhappy+superangry+casual+casualv2 
	            		&& randomEyebrowNumber<normalEyebrow+angry+sad+unhappy+superangry+casual+casualv2+normalv2)  {
	            	metadataEyebrows = "Normal v2";
	            	for(int i=0; i<35; i=i+10) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 680-(i/3), 20, 25);
	                    g2d.fillRect(420+i, 680-(i/3), 20, 25);
	                    g2d.fillRect(290-i, 670-(i/3), 20, 25);
	                    g2d.fillRect(470+i, 670-(i/3), 20, 25);
	                    g2d.fillRect(240-i, 660+(i/3), 20, 25);
	                    g2d.fillRect(520+i, 660+(i/3), 20, 25);
	                    g2d.fillRect(190-i, 670+(i/3), 20, 25);
	                    g2d.fillRect(570+i, 670+(i/3), 20, 25);
	                }
	            }
	            else if(randomEyebrowNumber>=normalEyebrow+angry+sad+unhappy+superangry+casual+casualv2+normalv2 
	            		&& randomEyebrowNumber<normalEyebrow+angry+sad+unhappy+superangry+casual+casualv2+normalv2+shocked) {
	            	metadataEyebrows = "Shocked";
	            	for(int i=0; i<35; i=i+10) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 640+(i), 20, 25);
	                    g2d.fillRect(420+i, 640+(i), 20, 25);
	                    g2d.fillRect(290-i, 670-(i/3), 20, 25);
	                    g2d.fillRect(470+i, 670-(i/3), 20, 25);
	                    g2d.fillRect(240-i, 660+(i/3), 20, 25);
	                    g2d.fillRect(520+i, 660+(i/3), 20, 25);
	                    g2d.fillRect(190-i, 670+(i), 20, 25);
	                    g2d.fillRect(570+i, 670+(i), 20, 25);
	                }   
	            }
	            else if(randomEyebrowNumber>=normalEyebrow+angry+sad+unhappy+superangry+casual+casualv2+normalv2+shocked 
	            		&& randomEyebrowNumber<normalEyebrow+angry+sad+unhappy+superangry+casual+casualv2+normalv2+shocked+dissappointed) {
	            	metadataEyebrows = "Disappointed";
	            	for(int i=0; i<35; i=i+10) {
	                	g2d.setColor(Color.black);
	                    g2d.fillRect(340-i, 640+(i), 20, 25);
	                    g2d.fillRect(420+i, 640+(i), 20, 25);
	                    g2d.fillRect(290-i, 670+(i/3), 20, 25);
	                    g2d.fillRect(470+i, 670+(i/3), 20, 25);
	                    g2d.fillRect(240-i, 680+(i), 20, 25);
	                    g2d.fillRect(520+i, 680+(i), 20, 25);
	                    g2d.fillRect(190-i, 710-(i), 20, 25);
	                    g2d.fillRect(570+i, 710-(i), 20, 25);
	                }    
	            }
      	}
		return bufferedImage;
	}

	@Override
	public BufferedImage glasses() {
        Color randomGlassColor = RainbowColors.getRandomColor(RainbowColors.allColors);
        Color randomGlassesColor = RainbowColors.getRandomColor(RainbowColors.allColors);
        
        
        int glassNormal = FrameGenerator.normalGlasses;
        int glassCrucked = FrameGenerator.cruckedGlasses;
        
        int randomGlassesNormal = random.nextInt(glassNormal-1, 10)+1;
        int randomGlassesCrucked = random.nextInt(glassCrucked-1, 10)+1;
     
        //jordans>1 && randomShoeNumber == 10
        
        if(glassNormal>1 && randomGlassesNormal == 10) {
        	metadataGlasses = "Normal Glasses";
        	for(int i = 200; i < 350; i=i+8) {
            	for(int j = 690; j < 880; j=j+8) {
            		g2d.setColor(randomGlassColor);
                    g2d.fillOval(i, j, 7, 7);
            	}
            }
        	for(int i = 440; i < 590; i=i+8) {
            	for(int j = 690; j < 880; j=j+8) {
                    g2d.fillOval(i, j, 7, 7);
            	}
            }
        	for(int i=0; i<160; i=i+1) {
            	g2d.setColor(randomGlassesColor);
                g2d.fillRect(200+i, 680-(i/20), 15, 20);
                g2d.fillRect(440+i, 680-(i/20), 15, 20);
                g2d.fillRect(180+i, 880-(i/20), 15, 20);
                g2d.fillRect(420+i, 880-(i/20), 15, 20);
                g2d.fillRect(350+(i/2), 780+(i/20), 15, 20);
            }
        	for(int i=0; i<20; i=i+1) {
                g2d.fillRect(180+i, 880-(i*10), 15, 20);
                g2d.fillRect(420+i, 880-(i*10), 15, 20);
                g2d.fillRect(340+i, 865-(i*10), 15, 20);
                g2d.fillRect(580+i, 865-(i*10), 15, 20);
            }
        	for(int i=0; i<130; i=i+10) {
                g2d.fillRect(190-i, 750-(i/3), 20, 25);
                g2d.fillRect(590+i, 760-(i/3), 20, 25);
            }	
        //////////////////////////////////////	
        }
        
        else if(glassCrucked>1 && randomGlassesCrucked == 10 && metadataGlasses.equals("none")) {
        	metadataGlasses = "Crucked Glasses";
        	//brilleglass venstre
        	for(int i = 215; i < 350; i=i+9) {
            	for(int j = 700; j < 880; j=j+8) {
            		g2d.setColor(randomGlassColor);
                    g2d.fillOval(i, j, 7, 7);
            	}
            }
        	//brilleglass h�gre
        	for(int i = 420; i < 560; i=i+9) {
            	for(int j = 700; j < 880; j=j+8) {
                    g2d.fillOval(i, j, 7, 7);
            	}
            }
        	for(int i=0; i<100; i=i+1) {
            	g2d.setColor(randomGlassesColor);
                g2d.fillRect(218+i, 690-(i/20), 15, 20);
                g2d.fillRect(430+i, 680-(i/20), 15, 20);
                g2d.fillRect(235+i, 880-(i/20), 15, 20);
                g2d.fillRect(410+i, 880-(i/20), 15, 20);
                g2d.fillRect(405-(i/2), 780+(i/20), 15, 20);
            }
        	for(int i=0; i<14; i=i+1) {
                g2d.fillRect(190+i, 860-(i*10), 15, 20);
                g2d.fillRect(400+i, 860-(i*10), 15, 20);
                g2d.fillRect(340+i, 855-(i*10), 15, 20);
                g2d.fillRect(560+i, 845-(i*10), 15, 20);
            }
        	for(int i=0; i<20; i=i+10) {
                g2d.fillRect(340-i, 700-(i/2), 20, 25);
                g2d.fillRect(420+i, 710-(i/2), 20, 25);
            }
        	for(int i=0; i<25; i=i+10) {
                g2d.fillRect(220-i, 865-(i/2), 20, 25);
                g2d.fillRect(520+i, 865-(i/2), 20, 25);
            }
            g2d.fillRect(215, 710, 20, 30);
            g2d.fillRect(555, 690, 20, 35);
            g2d.fillRect(535, 690, 20, 20);
            g2d.fillRect(423, 690, 20, 20);
            
            for(int i=0; i<130; i=i+10) {
                g2d.fillRect(190-i, 750-(i/3), 20, 25);
                g2d.fillRect(570+i, 750-(i/3), 20, 25);
            }       
        }
		return bufferedImage;
	}
	
	
	public BufferedImage clothes() {
		int low = 20;
        int high = 235;
        int shirtshadow1 = random.nextInt(high-low) + low;
        int shirtshadow2 = random.nextInt(high-low) + low;
        int shirtshadow3 = random.nextInt(high-low) + low;
        int bodyheight = 20;
        //skjorte
        g2d.setColor(Color.black);
        g2d.fillRect(287, 1397-bodyheight, 206, 186);
        g2d.fillRect(197, 1417-bodyheight, 81, 146);
        g2d.fillRect(507, 1417-bodyheight, 81, 146);
        g2d.fillRect(217, 1407-bodyheight, 356, 76);
        g2d.setColor(new Color(shirtshadow1, shirtshadow2, shirtshadow3));
        g2d.fillRect(290, 1400-bodyheight, 200, 180);
        g2d.fillRect(200, 1420-bodyheight, 75, 140);
        g2d.fillRect(510, 1420-bodyheight, 75, 140);
        g2d.fillRect(220, 1410-bodyheight, 350, 70);
        //abs
        g2d.setColor(new Color(shirtshadow1+20, shirtshadow2+20, shirtshadow3+20));
        g2d.fillRect(340, 1430-bodyheight, 100, 110);
        //abs stripe
        g2d.setColor(new Color(shirtshadow1-20, shirtshadow2-20, shirtshadow3-20));
        g2d.fillRect(388, 1497-bodyheight, 4, 38);
        g2d.fillRect(355, 1506-bodyheight, 70, 4);
        g2d.fillRect(355, 1523-bodyheight, 70, 4);
        //chest
        g2d.setColor(new Color(shirtshadow1-10, shirtshadow2-10, shirtshadow3-10));
        g2d.fillRect(295, 1420-bodyheight, 90, 75);
        g2d.fillRect(395, 1420-bodyheight, 90, 75);
        //inni chest
        g2d.setColor(new Color(shirtshadow1+10, shirtshadow2+10, shirtshadow3+10));
        g2d.fillRect(305, 1435-bodyheight, 70, 35);
        g2d.fillRect(405, 1435-bodyheight, 70, 35);
        g2d.fillRect(325, 1450-bodyheight, 30, 30);
        g2d.fillRect(425, 1450-bodyheight, 30, 30);
        //nippel
        g2d.setColor(new Color(shirtshadow1+20, shirtshadow2+20, shirtshadow3+20));
        g2d.fillRect(330, 1458-bodyheight, 20, 15);
        g2d.fillRect(430, 1458-bodyheight, 20, 15);
        //skulder
        g2d.fillRect(220, 1420-bodyheight, 50, 40);
        g2d.fillRect(515, 1420-bodyheight, 55, 40);
        g2d.fillRect(245, 1420-bodyheight, 40, 20);
        g2d.fillRect(500, 1420-bodyheight, 40, 20);
        //arm
        g2d.fillRect(255, 1490-bodyheight, 10, 40);
        g2d.fillRect(520, 1490-bodyheight, 10, 40);
        g2d.fillRect(210, 1470-bodyheight, 10, 60);
        g2d.fillRect(565, 1470-bodyheight, 10, 60);
        g2d.setColor(new Color(shirtshadow1-20, shirtshadow2-20, shirtshadow3-20));
        //ytterkant arm
        g2d.fillRect(200, 1420-bodyheight, 10, 140);
        g2d.fillRect(575, 1420-bodyheight, 10, 140);
        g2d.fillRect(265, 1460-bodyheight, 10, 100);
        g2d.fillRect(510, 1460-bodyheight, 10, 100);
        //ytterkant oppe og nede mage
        g2d.fillRect(290, 1400-bodyheight, 200, 10);
        g2d.fillRect(290, 1540-bodyheight, 200, 10);
        
        int pantshadow1 = random.nextInt(high-low) + low;
        int pantshadow2 = random.nextInt(high-low) + low;
        int pantshadow3 = random.nextInt(high-low) + low;
        //bukse
        g2d.setColor(Color.black);
        g2d.fillRect(287, 1550-bodyheight, 206, 36);
        g2d.fillRect(307, 1580-bodyheight, 76, 76);
        g2d.fillRect(397, 1580-bodyheight, 76, 76);
        g2d.setColor(new Color(pantshadow1, pantshadow2, pantshadow3));
        g2d.fillRect(290, 1553-bodyheight, 200, 30);
        g2d.fillRect(310, 1583-bodyheight, 70, 70);
        g2d.fillRect(400, 1583-bodyheight, 70, 70);
        g2d.setColor(new Color(pantshadow1-20, pantshadow2-20, pantshadow3-20));
        //�verst skal vere m�rk
        g2d.fillRect(290, 1553-bodyheight, 200, 10);
        g2d.fillRect(290, 1563-bodyheight, 10, 20);
        g2d.fillRect(320, 1563-bodyheight, 10, 25);
        g2d.fillRect(360, 1563-bodyheight, 10, 25);
        g2d.fillRect(410, 1563-bodyheight, 10, 25);
        g2d.fillRect(450, 1563-bodyheight, 10, 25);
        g2d.fillRect(480, 1563-bodyheight, 10, 20);
      //nederst skal vere m�rk
        g2d.fillRect(310, 1637-bodyheight, 70, 10);
        g2d.fillRect(400, 1637-bodyheight, 70, 10);
        //kne
        g2d.setColor(new Color(pantshadow1+20, pantshadow2+20, pantshadow3+20));
        g2d.fillRect(320, 1600-bodyheight, 50, 5);
        g2d.fillRect(410, 1600-bodyheight, 50, 5);
        g2d.fillRect(335, 1600-bodyheight, 20, 13);
        g2d.fillRect(425, 1600-bodyheight, 20, 13);
        ///////////
        g2d.setColor(new Color(pantshadow1+5, pantshadow2+5, pantshadow3+5));
        g2d.fillRect(320, 1563-bodyheight, 10, 20);
        g2d.fillRect(320, 1563-bodyheight, 10, 20);
        
        g2d.fillRect(360, 1563-bodyheight, 10, 20);
        
        g2d.fillRect(340, 1563-bodyheight, 10, 30);
        g2d.fillRect(430, 1563-bodyheight, 10, 30);
        
        g2d.fillRect(410, 1563-bodyheight, 10, 20);
        g2d.fillRect(450, 1563-bodyheight, 10, 20);
        //////////
        g2d.setColor(new Color(pantshadow1-10, pantshadow2-10, pantshadow3-10));
        //inner
        g2d.fillRect(320, 1627-bodyheight, 50, 10);
        g2d.fillRect(410, 1627-bodyheight, 50, 10);
        
        g2d.fillRect(330, 1563-bodyheight, 30, 10);
        g2d.fillRect(420, 1563-bodyheight, 30, 10);
        //boots
        int bootshadow1 = random.nextInt(high-low) + low;
        int bootshadow2 = random.nextInt(high-low) + low;
        int bootshadow3 = random.nextInt(high-low) + low;
        
        g2d.setColor(Color.black);
        g2d.fillRect(302, 1647-bodyheight, 86, 26);
        g2d.fillRect(392, 1647-bodyheight, 86, 26);
        g2d.setColor(new Color(bootshadow1+20, bootshadow2+20, bootshadow3+20));
        g2d.fillRect(305, 1650-bodyheight, 80, 20);
        g2d.fillRect(395, 1650-bodyheight, 80, 20);
        
        g2d.setColor(new Color(bootshadow1-20, bootshadow2-20, bootshadow3-20));
        g2d.fillRect(305, 1650-bodyheight, 10, 20);
        g2d.fillRect(375, 1650-bodyheight, 10, 20);
        g2d.fillRect(315, 1650-bodyheight, 60, 10);
        
        g2d.fillRect(395, 1650-bodyheight, 10, 20);
        g2d.fillRect(465, 1650-bodyheight, 10, 20);
        g2d.fillRect(405, 1650-bodyheight, 60, 10);
		return bufferedImage;
	}

	@Override
	public BufferedImage accessories() {
		//jordan 1
        List<Color> shoeColor = Arrays.asList(yellow, brown, red, green, lightBlue, blue, deepred, lightGreen, purple);
        Color randomShoeColor = shoeColor.get(random.nextInt(shoeColor.size()));
        
        int jordans = FrameGenerator.jordans;
        
        int randomShoeNumber = random.nextInt(jordans-1, 10)+1;
        
        int rightside = 20; 
        int leftside = -300;
        
        if(jordans>1 && randomShoeNumber == 10) {
        	//kvit ferdig
            g2d.setColor(Color.white);
            g2d.fillRect(500+rightside, 1645, 120, 5);
            g2d.fillRect(610+rightside, 1640, 25, 5);
            g2d.fillRect(630+rightside, 1635, 15, 5);
            g2d.fillRect(500+rightside, 1640, 25, 5);
            g2d.fillRect(545+rightside, 1640, 25, 5);
            g2d.fillRect(540+rightside, 1635, 30, 5);
            g2d.fillRect(500+rightside, 1600, 15, 5);
            g2d.fillRect(510+rightside, 1605, 55, 5);
            g2d.fillRect(520+rightside, 1610, 35, 5);
            g2d.fillRect(520+rightside, 1615, 40, 5);
            g2d.fillRect(565+rightside, 1630, 10, 5);
            g2d.fillRect(570+rightside, 1625, 10, 5);
            g2d.fillRect(570+rightside, 1620, 10, 5);
            g2d.fillRect(540+rightside, 1620, 15, 5);
            g2d.fillRect(535+rightside, 1630, 10, 5);
            g2d.fillRect(560+rightside, 1610, 10, 5);
            g2d.fillRect(565+rightside, 1615, 10, 5);
            g2d.fillRect(540+rightside, 1600, 10, 5);
            g2d.setColor(Color.black);
            g2d.fillRect(570+rightside, 1640, 40, 5);
            g2d.fillRect(570+rightside, 1635, 65, 5);
            g2d.fillRect(575+rightside, 1630, 25, 5);
            g2d.fillRect(580+rightside, 1625, 20, 5);
            g2d.fillRect(580+rightside, 1620, 20, 5);
            g2d.fillRect(575+rightside, 1615, 20, 5);
            g2d.fillRect(570+rightside, 1610, 15, 5);
            g2d.fillRect(565+rightside, 1605, 15, 5);
            g2d.fillRect(550+rightside, 1600, 20, 5);
            g2d.fillRect(525+rightside, 1600, 15, 5);
            g2d.fillRect(535+rightside, 1595, 25, 5);
            g2d.fillRect(545+rightside, 1590, 10, 5);
            g2d.fillRect(545+rightside, 1585, 5, 5);
            g2d.fillRect(545+rightside, 1580, 5, 5);
            g2d.fillRect(570+rightside, 1640, 40, 5);
            g2d.fillRect(640+rightside, 1625, 5, 5);
            g2d.fillRect(605+rightside, 1630, 40, 5);
            g2d.fillRect(600+rightside, 1620, 5, 5);
            g2d.fillRect(520+rightside, 1575, 20, 5);
            g2d.fillRect(510+rightside, 1580, 15, 5);
            g2d.fillRect(505+rightside, 1585, 10, 5);
            //swoosh
            g2d.setColor(Color.black);
            g2d.fillRect(545+rightside, 1630, 20, 5);
            g2d.fillRect(530+rightside, 1625, 40, 5);
            g2d.fillRect(525+rightside, 1620, 15, 5);
            g2d.fillRect(515+rightside, 1615, 15, 5);
            g2d.fillRect(510+rightside, 1610, 15, 5);
            g2d.fillRect(500+rightside, 1605, 10, 5);
            g2d.fillRect(555+rightside, 1620, 15, 5);
            g2d.fillRect(555+rightside, 1615, 10, 5);
            g2d.fillRect(555+rightside, 1610, 5, 5);
            //toe
            g2d.setColor(randomShoeColor);
            g2d.fillRect(600+rightside, 1630, 5, 5);
            g2d.fillRect(600+rightside, 1625, 40, 5);
            g2d.fillRect(605+rightside, 1620, 20, 5);
            //high topp
            g2d.fillRect(500+rightside, 1595, 35, 5);
            g2d.fillRect(515+rightside, 1600, 10, 5);
            g2d.fillRect(505+rightside, 1590, 40, 5);
            g2d.fillRect(515+rightside, 1585, 30, 5);
            g2d.fillRect(525+rightside, 1580, 20, 5);
            g2d.fillRect(540+rightside, 1575, 5, 5);
            //s�lefarge
            g2d.fillRect(500+rightside, 1650, 120, 5);
            g2d.fillRect(620+rightside, 1645, 15, 5);
            g2d.fillRect(635+rightside, 1640, 10, 5);
            //skofarge hel bak
            g2d.fillRect(525+rightside, 1640, 20, 5);
            g2d.fillRect(500+rightside, 1635, 40, 5);
            g2d.fillRect(500+rightside, 1630, 35, 5);
            g2d.fillRect(500+rightside, 1625, 35, 5);
            g2d.fillRect(500+rightside, 1620, 30, 5);
            g2d.fillRect(500+rightside, 1615, 25, 5);
            g2d.fillRect(500+rightside, 1610, 10, 5);
        
        
        	g2d.setColor(Color.white);
            g2d.fillRect(500+leftside, 1645, 120, 5);
            g2d.fillRect(610+leftside, 1640, 25, 5);
            g2d.fillRect(630+leftside, 1635, 15, 5);
            g2d.fillRect(500+leftside, 1640, 25, 5);
            g2d.fillRect(545+leftside, 1640, 25, 5);
            g2d.fillRect(540+leftside, 1635, 30, 5);
            g2d.fillRect(500+leftside, 1600, 15, 5);
            g2d.fillRect(510+leftside, 1605, 55, 5);
            g2d.fillRect(520+leftside, 1610, 35, 5);
            g2d.fillRect(520+leftside, 1615, 40, 5);
            g2d.fillRect(565+leftside, 1630, 10, 5);
            g2d.fillRect(570+leftside, 1625, 10, 5);
            g2d.fillRect(570+leftside, 1620, 10, 5);
            g2d.fillRect(540+leftside, 1620, 15, 5);
            g2d.fillRect(535+leftside, 1630, 10, 5);
            g2d.fillRect(560+leftside, 1610, 10, 5);
            g2d.fillRect(565+leftside, 1615, 10, 5);
            g2d.fillRect(540+leftside, 1600, 10, 5);
            //g2d.fillRect(535, 1630, 40, 5);
            g2d.setColor(Color.black);
            g2d.fillRect(570+leftside, 1640, 40, 5);
            g2d.fillRect(570+leftside, 1635, 65, 5);
            g2d.fillRect(575+leftside, 1630, 25, 5);
            g2d.fillRect(580+leftside, 1625, 20, 5);
            g2d.fillRect(580+leftside, 1620, 20, 5);
            g2d.fillRect(575+leftside, 1615, 20, 5);
            g2d.fillRect(570+leftside, 1610, 15, 5);
            g2d.fillRect(565+leftside, 1605, 15, 5);
            g2d.fillRect(550+leftside, 1600, 20, 5);
            g2d.fillRect(525+leftside, 1600, 15, 5);
            g2d.fillRect(535+leftside, 1595, 25, 5);
            g2d.fillRect(545+leftside, 1590, 10, 5);
            g2d.fillRect(545+leftside, 1585, 5, 5);
            g2d.fillRect(545+leftside, 1580, 5, 5);
            g2d.fillRect(570+leftside, 1640, 40, 5);
            g2d.fillRect(640+leftside, 1625, 5, 5);
            g2d.fillRect(605+leftside, 1630, 40, 5);
            g2d.fillRect(600+leftside, 1620, 5, 5);
            g2d.fillRect(520+leftside, 1575, 20, 5);
            g2d.fillRect(510+leftside, 1580, 15, 5);
            g2d.fillRect(505+leftside, 1585, 10, 5);
            //swoosh
            g2d.setColor(Color.black);
            g2d.fillRect(545+leftside, 1630, 20, 5);
            g2d.fillRect(530+leftside, 1625, 40, 5);
            g2d.fillRect(525+leftside, 1620, 15, 5);
            g2d.fillRect(515+leftside, 1615, 15, 5);
            g2d.fillRect(510+leftside, 1610, 15, 5);
            g2d.fillRect(500+leftside, 1605, 10, 5);
            g2d.fillRect(555+leftside, 1620, 15, 5);
            g2d.fillRect(555+leftside, 1615, 10, 5);
            g2d.fillRect(555+leftside, 1610, 5, 5);
            //toe
            g2d.setColor(randomShoeColor);
            g2d.fillRect(600+leftside, 1630, 5, 5);
            g2d.fillRect(600+leftside, 1625, 40, 5);
            g2d.fillRect(605+leftside, 1620, 20, 5);
            //high topp
            g2d.fillRect(500+leftside, 1595, 35, 5);
            g2d.fillRect(515+leftside, 1600, 10, 5);
            g2d.fillRect(505+leftside, 1590, 40, 5);
            g2d.fillRect(515+leftside, 1585, 30, 5);
            g2d.fillRect(525+leftside, 1580, 20, 5);
            g2d.fillRect(540+leftside, 1575, 5, 5);
            //s�lefarge
            g2d.fillRect(500+leftside, 1650, 120,5);
            g2d.fillRect(620+leftside, 1645, 15, 5);
            g2d.fillRect(635+leftside, 1640, 10, 5);
            //skofarge hel bak
            g2d.fillRect(525+leftside, 1640, 20, 5);
            g2d.fillRect(500+leftside, 1635, 40, 5);
            g2d.fillRect(500+leftside, 1630, 35, 5);
            g2d.fillRect(500+leftside, 1625, 35, 5);
            g2d.fillRect(500+leftside, 1620, 30, 5);
            g2d.fillRect(500+leftside, 1615, 25, 5);
            g2d.fillRect(500+leftside, 1610, 10, 5);
        }
		return bufferedImage;
	}
	
	/**
	 * draws the BufferedImages in layers starting with background, creature, mouth
	 * eyes, hair, alienantenna, nose, eyebrows, glasses, clothes, assesories.
	 * @param background
	 * @param creature
	 * @param mouth
	 * @param eyes
	 * @param hair
	 * @param alienAntenna
	 * @param nose
	 * @param eyebrows
	 * @param glasses
	 * @param clothes
	 * @param accessories
	 * @return 
	 */
	public static BufferedImage joinBufferedImage(BufferedImage background,
			                                      BufferedImage creature,
			                                      BufferedImage mouth,
			                                      BufferedImage eyes,
			                                      BufferedImage hair,
			                                      BufferedImage alienAntenna,
	                                              BufferedImage nose,
	                                              BufferedImage eyebrows,
	                                              BufferedImage glasses,
	                                              BufferedImage clothes,
	                                              BufferedImage accessories){
		    int width = 780;
		    int height = 1688;
		    BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		    Graphics2D g2d = newImage.createGraphics();
		    g2d.drawImage(background, null, 0, 0);
		    g2d.drawImage(creature, null, 0, 0);
		    g2d.drawImage(mouth, null, 0, 0);
		    g2d.drawImage(eyes, null, 0, 0);
		    g2d.drawImage(hair, null, 0, 0);
		    g2d.drawImage(alienAntenna, null, 0, 0);
		    g2d.drawImage(nose, null, 0, 0);
		    g2d.drawImage(eyebrows, null, 0, 0);
		    g2d.drawImage(glasses, null, 0, 0);
		    g2d.drawImage(clothes, null, 0, 0);
		    g2d.drawImage(accessories, null, 0, 0);
		    g2d.dispose();
		    return newImage;
		  }
	
	

}
