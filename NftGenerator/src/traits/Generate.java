package traits;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import FileModification.MetadataModification;
import FileModification.RenameFile;
import Frames.FrameGenerator;

public class Generate {
	
	//To get the program to work you need to update the static field varibles:
	//filePath and metadataFilePath inside the Generator class that is located in the traits directory.
	//These varibles needs to be updated such that it matches your current computer location. Example:
	//static String filePath = "C:\\Users\\Magnus Eveb�\\eclipse-workspace\\NftGenerator/";
	//static String metadataFilePath = "C:\\Users\\Magnus Eveb�\\eclipse-workspace/metadata.json";
	
	//another example:
	//static String filePath =         "C:\\Users\\Magnus Eveb�\\gitbash\\nft-generator\\NftGenerator/";
	//static String metadataFilePath = "C:\\Users\\Magnus Eveb�\\gitbash\\nft-generator/metadata.json";
	
	//filePath should end with:         "\\nft-generator\\NftGenerator/"
	//metadataFilePath should end with: "\\nft-generator\metadata.json/"
	
	static String filePath =         "C:\\Users\\magnus\\Documents\\1SKULEN\\UIB\\semesteroppgave2\\nft-generator\\NftGenerator/";
	static String metadataFilePath = "C:\\Users\\magnus\\Documents\\1SKULEN\\UIB\\semesteroppgave2\\nft-generator/metadata.json";
	
	/**
	 * filePath for the original metadata templete needs to be specified
	 * filePath for the output of the png and json files need to be specified
	 * 
	 * Uses the Trait code to first layer all the BufferedImages on top 
	 * of each other starting with; background, body, mouth, eyes, hair,
	 * alienantenna, nose, eyebrows, glasses, clothes and assessories.
	 * The merged picture then gets written to a png file named 0.png
	 * 1.png 2.png ... n.png. There are also created a metadata file
	 * corresponding to each trait that is named 0.json 1.json
	 * 2.json ... n.json.  
	 */
	public static void generate() {
		int iterations = 0;		
	    while(iterations<FrameGenerator.numberOfNFTS) {
	    	String fileName = String.valueOf(iterations);
			int width = 780;
			int height = 1688;
			BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			Traits traits = new Traits(width, height);
			bufferedImage = Traits.joinBufferedImage(traits.background(),
					                                 traits.body(),
					                                 traits.mouth(),
					                                 traits.eyes(),
					                                 traits.hair(),
					                                 traits.alienAntenna(),
					                                 traits.nose(),
					                                 traits.eyebrows(),
					                                 traits.glasses(),
					                                 traits.clothes(),
					                                 traits.accessories());

			RenameFile.renameFile(metadataFilePath, fileName+".json");
			MetadataModification.modifyFile(filePath+fileName+".json", "xxxxx", String.valueOf(FrameGenerator.numberOfNFTS));
	        MetadataModification.modifyFile(filePath+fileName+".json", "yyyyy", String.valueOf(FrameGenerator.numberOfNFTS));
	        MetadataModification.modifyFile(filePath+fileName+".json", "00000", String.valueOf(iterations+1));
	        MetadataModification.modifyFile(filePath+fileName+".json", "aaaaa", String.valueOf(iterations+1));
	        MetadataModification.modifyFile(filePath+fileName+".json", "bbbbb", fileName);
	        MetadataModification.modifyFile(filePath+fileName+".json", "11111", traits.metadataBackground);
	        MetadataModification.modifyFile(filePath+fileName+".json", "uuuuu", traits.metadataBody);
	        MetadataModification.modifyFile(filePath+fileName+".json", "22222", traits.metadataEye);
	        MetadataModification.modifyFile(filePath+fileName+".json", "33333", traits.metadataMouth);
		    MetadataModification.modifyFile(filePath+fileName+".json", "44444", traits.metadataHair);
	        MetadataModification.modifyFile(filePath+fileName+".json", "55555", traits.metadataEyebrows);
	        MetadataModification.modifyFile(filePath+fileName+".json", "66666", traits.metadataGlasses);
	        MetadataModification.modifyFile(filePath+fileName+".json", "ccccc", fileName);
			File file = new File(String.valueOf(iterations)+".png");
	        try {
				ImageIO.write(bufferedImage, "png", file);
			} catch (IOException e) {
				e.printStackTrace();
			}
	        iterations++;
	    }
	   
	
	}

}
