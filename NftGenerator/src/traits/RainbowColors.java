package traits;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class RainbowColors {
	static Random random = new Random();
	
////////////////////COLORS /////////////////////////////
public static Color black = new Color(0, 0, 0);
public static Color lightBlack = new Color(38, 38, 38);
public static Color white = new Color(255, 255, 255);

public static Color darkGrey = new Color(58, 58, 58);
public static Color grey = new Color(210, 209, 204);
public static Color lightGrey = new Color(134, 134, 134);


public static Color darkRed = new Color(191, 10, 48);
public static Color red = new Color(211, 47, 47);
public static Color lightRed = new Color(255, 71, 71);


public static Color lightYellow = new Color(255, 255, 102);
public static Color yellow = new Color(255, 255, 102);
public static Color darkYellow = new Color(255, 255, 0);


public static Color lightBrown = new Color(127, 100, 93);
public static Color brown = new Color(145, 122, 116);
public static Color darkBrown = new Color(72, 34, 24);


public static  Color darkBlue = new Color(3, 37, 76);
public static Color lightBlue = new Color(90, 138, 211);
public static Color blue = new Color(83, 136, 255);


public static Color lightGreen = new Color(76, 175, 80);
public static Color green = new Color(61, 158, 11);
public static Color darkGreen = new Color(18, 52, 41);


public static Color lightPurple = new Color(207, 159, 229);
public static Color purple = new Color(158, 53, 224);
public static Color darkPurple = new Color(69, 11, 112);


public static Color lightOrange = new Color (183, 153, 6);
public static Color orange = new Color (255, 132, 38);
public static Color darkOrange = new Color (255, 161, 89);


public static Color darkGold = new Color (183, 153, 6);
public static Color gold = new Color (239, 197, 1);
public static Color lightGold = new Color (246, 213, 109);

public static Color greenBlue = new Color (57, 240, 119);
public static Color blueGreen = new Color (0, 201, 167);
////////////////////COLORS //////////////////////////////////

/////// skin Colors /////////////////////

public static Color skintone1 = new Color (197, 140, 133);
public static Color skincontrast1 = new Color (197+20, 140+20, 133+20);
public static List<Color> skin1 = Arrays.asList(skintone1, skincontrast1);

public static Color skintone2 = new Color (236, 188, 180);
public static Color skincontrast2 = new Color (236-20, 188-20, 180-20);
public static List<Color> skin2 = Arrays.asList(skintone2, skincontrast2);

public static Color skintone3 = new Color (209, 163, 164);
public static Color skincontrast3 = new Color (209+20, 163+20, 164+20);
public static List<Color> skin3 = Arrays.asList(skintone3, skincontrast3);

public static Color skintone4 = new Color (161, 102, 94);
public static Color skincontrast4 = new Color (161-20, 102-20, 94-20);
public static List<Color> skin4 = Arrays.asList(skintone4, skincontrast4);

public static Color skintone5 = new Color (80, 51, 53);
public static Color skincontrast5 = new Color (80+20, 51+20, 53+20);
public static List<Color> skin5 = Arrays.asList(skintone5, skincontrast5);

public static Color skintone6 = new Color (161, 102, 94);
public static Color skincontrast6 = new Color (161+20, 102+20, 94+20);
public static List<Color> skin6 = Arrays.asList(skintone6, skincontrast6);

@SuppressWarnings("rawtypes")
public static List<List> allSkin = Arrays.asList(skin1, skin2, skin3, skin4, skin5, skin6);

/////// skin Colors ///////////////////////////////////////
public static Color getSkinTone(List<Color> skin) {
	return skin.get(0);
}
public static Color getSkinContrast(List<Color> skin) {
	return skin.get(1);
}
@SuppressWarnings({ "rawtypes", "unchecked" })
public static List<Color> getRandomSkin(List<List> input) {
	int i = random.nextInt(input.size());
	return input.get(i);
}


public static List<Color> allColors = Arrays.asList(black,lightBlack, white,
									darkGrey, grey, lightGrey,
									darkRed, red, lightRed,
									darkYellow, yellow, lightYellow,
									darkBrown, brown, lightBrown,
									darkBlue, blue, lightBlue,
									darkPurple, purple, lightPurple,
									darkOrange, orange, lightOrange,
									darkGold, gold, lightGold,
									greenBlue, blueGreen
									);



public static Color getRandomColor(List<Color> colorList) {
	Color randomColor = colorList.get(random.nextInt(colorList.size()));
	return randomColor;	
}



}



