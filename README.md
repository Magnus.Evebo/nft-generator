
#NFT-Generator:

NFT generator
Non-fungible token picture(png-file) and metadata(json-file) generator.


#Visuals: 

https://www.youtube.com/watch?v=SrZEH8LVmQs

A sample of 100 NFT's uploaded to the solana Devnet: https://explorer.solana.com/address/9L2ymJMXXTMrUZAZCy6nTDUL2SWcWPRh4xEtUSqJPezF/tokens?cluster=devnet under "Token Holdings" you can click on Number #Number 48, Number #Number 60, Number #Number 100 ... and see the NFT's and on each NFT page under "Attributes" you can see the trait attributes.

All the png pictures are 780x1688 (iphone 11, iphone 12, iphone 13 screen size ratio).


#Installation:
 
To get the program to work you need to update the static field varibles: filePath and metadataFilePath inside the Generator class that is located in the traits directory. These varibles needs to be updated such that it matches your current computer location. Example: "static String filePath = "C:\Users\Magnus Evebø\eclipse-workspace\NftGenerator/";" and "static String metadataFilePath = "C:\Users\Magnus Evebø\eclipse-workspace/metadata.json";". Just follow the steps in the YouTube video. 


#Usage:
 
When you first run the program the Main Menu JFrame pops up, here you have the option to choose how many NFT's you want to generate by using the vertical slider on the rigth side. You have the generate button that when pressed generates the nft-png-files and the metadata-json-files. The other buttons with the different traits takes you to a new frame where you can choose how rare each trait in each layer should be (ranging from 0-10, 0 not at all, 1 low drop rate, 10 high drop rate). The metadata file is made for the solana blockchain. https://docs.metaplex.com/candy-machine-v2/preparing-assets. When generating a large number of NFT's the program can be a bit slow. It took me about 2 minutes to generate 1000 NFT's.


#Authors and acknowledgment:
 
Magnus Evebø


#Testing:
 
I have mamually tested by clicking on almost all possible combinations inside the program and it works without crashing. The only bug I did not manage to fix is when you are on the Creature JFrame the preview Creature button does not refresh when clicked, the reason for this is the rezise method(located in the FrameGenerator class). When you use the slider and chooses 0 there will be a 0% drop chance. This is not a bug, because if you decide you do not want all the different traits you are free to not incude them. If you choose to generate a large number of NFT's the program will be unresponsive untill all the NFT's are generated.  


#Project status:
 
Finished, but room for added features. The JMenuBar does not have a actionlistner so it does not do anything. It is there to get a better overview of the traits.
